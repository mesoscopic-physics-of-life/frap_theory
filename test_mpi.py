from fem_sol import frap_solver
f_0 = frap_solver([4, 4, 0.5], 'Meshes/single_drop_coverslip.xml',
                          name='FRAP_coverslip', T=300, phi_tot_ext=0.001)
f_0.solve_frap()
