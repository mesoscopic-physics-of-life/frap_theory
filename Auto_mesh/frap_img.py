from collections import OrderedDict
from copy import deepcopy
from math import *
from scipy import ndimage, misc
from scipy.interpolate import RegularGridInterpolator
from scipy.signal import savgol_filter
from scipy.spatial import distance
import matplotlib.pyplot as plt
import numpy as np
import tifffile


def find_sigma(FRAP,tif_frame, tif_frame_after_frap ,sigma):
    '''
    This function return the images of the frames selected before and 
    after the image analysis, in function of the parameters sigma.
    In order to have the better contours we should have the contours 
    well defined in black after the treatement of each frames. 
    The parameter FRAP allow to choose if we want (1) or not (0) to plot 
    the result of the after FRAP frame.
    '''

    img_beforeFRAP = ndimage.gaussian_laplace(tif_frame,sigma)
    plt.imshow(tif_frame)
    plt.title('Experimental data')
    bar = plt.colorbar() 
    bar.set_label('Intensity',size=14)
    plt.show()
    plt.imshow(img_beforeFRAP)
    plt.title('Gaussian Filter and Laplace filter')
    bar = plt.colorbar() 
    bar.set_label('Intensity',size=14)
    plt.show()
    img_afterFRAP = 0

    if FRAP == 1:
        img_afterFRAP = ndimage.gaussian_laplace(tif_frame_after_frap,sigma)
        plt.imshow(tif_frame_after_frap)
        plt.title('Experimental data')
        bar = plt.colorbar() 
        bar.set_label('Intensity',size=14)
        plt.show()
        plt.imshow(img_afterFRAP)
        plt.title('Gaussian Filter and Laplace filter')
        bar = plt.colorbar() 
        bar.set_label('Intensity',size=14)
        plt.show()
    return img_beforeFRAP, img_afterFRAP



def contour_from_tif(FRAP,tif_frame, tif_frame_after_frap ,sigma, d, mesh_file, lc_min,
                     lc_min_selected, lc_min_frap,z,number_pixel,N):
    '''
    This function create the meshing from the frames choose, sigma need to be
    well initialize in that case. We choose a contour where the meshing will be better.
    The parameter FRAP allow to choose if we want (1) or not (0) to mesh the area in 
    the droplet create by the FRAP.
    The parameters d allow to choose a distance where we assume that 2 points are in 
    the same object.
    The mesh created will be named mesh_file.
    The parameters lc_min allows to choose the precision for respectively, 
    all the contour, the contour selected, and the contour create by the FRAP.
    As the mesh size is normalized we need to give the number of pixel in axis of the image
    (The image is supposed to be a square of pixels)
    The parameters z allow to choose the height of the mesh.
    When we use the FRAP we can have a lot of points, in order to save computing time when 
    the mesh is done we divide the number of points by N.
    '''
    # mesh parameters all contours
    lc_max = 1
    d_min_mesh = 0.03
    d_max_mesh = 0.3
    
    # mesh parameters selected contours
    lc_max_selected = 0.5
    d_min_mesh_selected = 0.002
    d_max_mesh_selected = 1
    
    # mesh parameters FRAP contours
    lc_max_frap = 0.5
    d_min_frap = 0.002
    d_max_frap = 1
      
    img_beforeFRAP, img_afterFRAP = find_sigma(FRAP,tif_frame, tif_frame_after_frap ,sigma)
    
    MIN =np.min(img_beforeFRAP)
    Mean = np.mean(img_beforeFRAP)

    Contours_x = []
    Contours_y = []
    
    # thresold
    img_beforeFRAP[img_beforeFRAP > (Mean-MIN)/2] = 0
    Contours_y, Contours_x = np.nonzero(img_beforeFRAP)

    
    coords_beforeFRAP = np.eye(int(len(Contours_x)),2)
    
    for i in range(int(len(Contours_x))):
        
        coords_beforeFRAP[i][0] = Contours_x[i]
        coords_beforeFRAP[i][1] = Contours_y[i]
        
    coords_beforeFRAP_bis = deepcopy(coords_beforeFRAP)
    list_objects = []
    keep_points = []
    while coords_beforeFRAP.shape[0] != 0:
        count = 0
        list_points = []
        sP = coords_beforeFRAP
        pA = coords_beforeFRAP[0,:] 
        distances = np.linalg.norm(sP - pA, ord=2, axis=1.)  # 'distances' is a list
        keep_points = [i for i,x in enumerate(distances) if x<=d]
        keep_coord = [coords_beforeFRAP[i] for i in keep_points]        
        if len(keep_coord) != 0:
            A = ((coords_beforeFRAP_bis[:,None] == keep_coord).all(2).any(1))
            img_beforeFRAP = np.where(A == True)
            list_points = list_points + img_beforeFRAP[0].tolist()
            list_points = list(OrderedDict.fromkeys(list_points))
            coords_beforeFRAP = np.delete(coords_beforeFRAP, keep_points, axis=0)
        while count != len(list_points) :
            c = 0
            sP = coords_beforeFRAP
            if coords_beforeFRAP.shape[0] != 0:
                pA = coords_beforeFRAP_bis[list_points[count],:]                
                distances = np.linalg.norm(sP - pA, ord=2, axis=1.)  # 'distances' is a list
                keep_points = [i for i,x in enumerate(distances) if x<=d]
                keep_coord = [coords_beforeFRAP[i] for i in keep_points]
                if len(keep_coord) != 0:
                    A = ((coords_beforeFRAP_bis[:,None] == keep_coord).all(2).any(1))
                    A = np.array(A)
                    img_beforeFRAP = np.where(A == True)
                    list_points = list_points + img_beforeFRAP[0].tolist()
                    list_points = list(OrderedDict.fromkeys(list_points))
                    coords_beforeFRAP = np.delete(coords_beforeFRAP, keep_points, axis=0)
            count = count +1
        list_objects.append(list_points)

    Y = []
    X = []
    for j in range(len(list_objects)):
        for i in range(len(list_objects[j])):
            Y.append(coords_beforeFRAP_bis[list_objects[j][i]][0])
            X.append(coords_beforeFRAP_bis[list_objects[j][i]][1])
        plt.scatter(Y,X, label = "%i" %j)
        Y = []
        X = []
        
    plt.axis([0,207, 207, 0])
    plt.gca().set_aspect('equal', adjustable='box')
    plt.legend()
    plt.title('Contours')
    plt.show()
        
    print('Choose a contour:') 
    choose = input()     
    
    if FRAP == 1:
        MIN =np.min(img_afterFRAP)
        Mean = np.mean(img_afterFRAP)
        Contours_x = []
        Contours_y = []
    
        # threshold
        img_afterFRAP[img_afterFRAP > (Mean-MIN)/2] = 0
        Contours_y, Contours_x = np.nonzero(img_afterFRAP)

        coords_afterFRAP = np.eye(int(len(Contours_x)),2)    
        for i in range(int(len(Contours_x))):
            coords_afterFRAP[i][0] = Contours_x[i]
            coords_afterFRAP[i][1] = Contours_y[i]
        
        coords_afterFRAP_bis = deepcopy(coords_afterFRAP)
        
        list_objects2 = []           
        keep_points = []
        count = 0
        list_points = []
        sP = coords_afterFRAP
        pA = coords_beforeFRAP_bis[list_objects[int(choose)][0]][:]
        distances = np.linalg.norm(sP - pA, ord=2, axis=1.)  # 'distances' is a list
        keep_points = [i for i,x in enumerate(distances) if x<=d]
        keep_coord = [coords_afterFRAP[i] for i in keep_points]  
        
        if len(keep_coord) != 0:
            A = ((coords_afterFRAP_bis[:,None] == keep_coord).all(2).any(1))
            A = np.array(A)
            img_beforeFRAP = np.where(A == True)
            list_points = list_points + img_beforeFRAP[0].tolist()
            list_points = list(OrderedDict.fromkeys(list_points))
            coords_afterFRAP = np.delete(coords_afterFRAP, keep_points, axis=0)
            
        while count != len(list_points) :
            c = 0
            sP = coords_afterFRAP
            if coords_afterFRAP.shape[0] != 0:
                pA = coords_afterFRAP_bis[list_points[count],:]                
                distances = np.linalg.norm(sP - pA, ord=2, axis=1.)  # 'distances' is a list
                keep_points = [i for i,x in enumerate(distances) if x<=d]
                keep_coord = [coords_afterFRAP[i] for i in keep_points]
                if len(keep_coord) != 0:
                    A = ((coords_afterFRAP_bis[:,None] == keep_coord).all(2).any(1))
                    A = np.array(A)
                    img_beforeFRAP = np.where(A == True)
                    list_points = list_points + img_beforeFRAP[0].tolist()
                    list_points = list(OrderedDict.fromkeys(list_points))
                    coords_afterFRAP = np.delete(coords_afterFRAP, keep_points, axis=0)
            count = count +1
            
        Y = []
        X = []
        for j in range(len(list_objects)):
            if j == int(choose):
                for k in range(0, int(len(list_points)-1-N),N):
                    Y.append(coords_afterFRAP_bis[list_points[k]][0])
                    X.append(coords_afterFRAP_bis[list_points[k]][1])
            for i in range(len(list_objects[j])):
                Y.append(coords_beforeFRAP_bis[list_objects[j][i]][0])
                X.append(coords_beforeFRAP_bis[list_objects[j][i]][1])
            plt.scatter(Y,X, label = "%i" %j)
            Y = []
            X = []
        plt.axis([0,207, 207, 0])
        plt.gca().set_aspect('equal', adjustable='box')
        plt.legend()
        plt.title('Contours')
        plt.show()

    # We create the file
    f = open(mesh_file, 'w')
    # Creation of the geometry
    f.write('//+\n' )
    f.write('lc = 1;\n' )
    f.write('//+\n' )
    f.write('Point(4) = {0, 1, 0, lc};\n' )
    f.write('//+\n' )
    f.write('Point(3) = {1, 1, 0, lc};\n' )
    f.write('//+\n' )
    f.write('Point(2) = {1, 0, 0, lc};\n' )
    f.write('//+\n' )
    f.write('Point(1) = {0, 0, 0, lc};\n' )
    f.write('//+\n' )
    f.write('Point(8) = {0, 1, '+ str(z)+', lc};\n' )
    f.write('//+\n' )
    f.write('Point(7) = {1, 1, '+ str(z)+', lc};\n' )
    f.write('//+\n' )
    f.write('Point(6) = {1, 0, '+ str(z)+', lc};\n' )
    f.write('//+\n' )
    f.write('Point(5) = {0, 0, '+ str(z)+', lc};\n' )
    f.write('//+\n' )
    f.write('Line(6) = {2, 1};\n' )
    f.write('//+\n' )
    f.write('Line(5) = {3, 2};\n' )
    f.write('//+\n' )
    f.write('Line(8) = {4, 3};\n' )
    f.write('//+\n' )
    f.write('Line(7) = {1, 4};\n' )
    f.write('//+\n' )
    f.write('Line(3) = {6, 5};\n' )
    f.write('//+\n' )
    f.write('Line(2) = {7, 6};\n' )
    f.write('//+\n' )
    f.write('Line(1) = {8, 7};\n' )
    f.write('//+\n' )
    f.write('Line(4) = {5, 8};\n' )
    f.write('//+\n' )
    f.write('Line(30000000) = {5, 1};\n' )
    f.write('//+\n' )
    f.write('Line(10000000) = {2, 6};\n' )
    f.write('//+\n' )
    f.write('Line(9) = {3, 7};\n' )
    f.write('//+\n' )
    f.write('Line(20000000) = {8, 4};\n' )
    f.write('//+\n' )
    f.write('Line Loop(13) = {9, 2, -10000000, -5};\n' )
    f.write('//+\n' )
    f.write('Plane Surface(14) = {13};\n' )
    f.write('//+\n' )
    f.write('Line Loop(15) = {1, -9, -8, -20000000};\n' )
    f.write('//+\n' )
    f.write('Plane Surface(16) = {15};\n' )
    f.write('//+\n' )
    f.write('Line Loop(17) = {8, 5, 6, 7};\n' )
    f.write('//+\n' )
    f.write('Plane Surface(18) = {17};\n' )
    f.write('//+\n' )
    f.write('Line Loop(19) = {3, 30000000, -6, 10000000};\n' )
    f.write('//+\n' )
    f.write('Plane Surface(20) = {19};\n' )
    f.write('//+\n' )
    f.write('Line Loop(21) = {30000000, 7, -20000000, -4};\n' )
    f.write('//+\n' )
    f.write('Plane Surface(22) = {21};\n' )
    f.write('//+\n' )
    f.write('Line Loop(23) ={2, 3, 4, 1};\n' )
    f.write('//+\n' )
    f.write('Plane Surface(24) = {-23};\n' )
    f.write('//+\n' )
    f.write('Surface Loop(25) = {24, 14, 16, 18, 20, 22};\n' )
    f.write('//+\n' )
    f.write('Volume(26) = {25};\n' )
    f.write('//+\n' )
    f.write('Physical Surface(27) = {16};\n' )
    f.write('//+\n' )
    f.write('Physical Surface(28) = {20};\n' )
    f.write('//+\n' )
    f.write('Physical Volume(29) = {26};\n' )
    f.write('//+\n' )
    f.write('Physical Surface(30) = {24};\n' )
    f.write('//+\n' )
    f.write('Physical Surface(31) = {18};\n' )
    f.write('//+\n' )
    
    # Creation of the FRAP points
    if FRAP ==1:
        for j in range(0,int((len(list_points))),N):
            f.write('Point(3000'+str(j)+') = {'+str(((coords_afterFRAP_bis[list_points[j]][0]/number_pixel)))+', '+
                    str((1-(coords_afterFRAP_bis[list_points[j]][1]/number_pixel)))+', 0, lc};\n' )
            f.write('//+\n' )
            f.write('Point('+str(4000*int(z+1))+str(j)+') = {'+str(((coords_afterFRAP_bis[list_points[j]][0]/number_pixel)))+
                    ', '+ str((1-(coords_afterFRAP_bis[list_points[j]][1]/number_pixel)))+','+ str(z)+', lc};\n' )
            f.write('//+\n' )
            
    # Creation of the contours points
    for i in range(len(list_objects)):
        for j in range(len(list_objects[i])):

            f.write('Point('+str(i+1)+str(j)+') = {'+str(((coords_beforeFRAP_bis[list_objects[i][j]][0]/number_pixel)))+', '+
                    str((1-(coords_beforeFRAP_bis[list_objects[i][j]][1]/number_pixel)))+', 0, lc};\n' )
            f.write('//+\n' )
            
            f.write('Point('+str(1000*int(z+1))+str(i+1)+str(j)+') = {'+str(((coords_beforeFRAP_bis[list_objects[i][j]][0]/number_pixel)))+
                    ', '+ str((1-(coords_beforeFRAP_bis[list_objects[i][j]][1]/number_pixel)))+','+ str(z)+', lc};\n' )
            f.write('//+\n' )
            
    # Creation of the contours Lines
    for i in range(len(list_objects)):
        for j in range(len(list_objects[i])-1):
            if sqrt((coords_beforeFRAP_bis[list_objects[i][j]][1] - coords_beforeFRAP_bis[list_objects[i][j+1]][1])*(coords_beforeFRAP_bis[list_objects[i][j]][1] -coords_beforeFRAP_bis[list_objects[i][j+1]][1]) + ((coords_beforeFRAP_bis[list_objects[i][j]][0] -  coords_beforeFRAP_bis[list_objects[i][j+1]][0])*(coords_beforeFRAP_bis[list_objects[i][j]][0] - coords_beforeFRAP_bis[list_objects[i][j+1]][0])))< d :
                f.write('Line('+str(i+1)+str(j)+') = {'+str(i+1)+str(j)+','+str(i+1)+str(j+1)+'};\n' )
                f.write('//+\n' )
                
                f.write('Line('+str(1000*int(z+1))+str(i+1)+str(j)+') = {'+str(1000*int(z+1))+str(i+1)+str(j)+','+str(1000*int(z+1))+str(i+1)+str(j+1)+'};\n' )
                f.write('//+\n' )
                
                f.write('Line('+str(2000*int(z+1))+str(i+1)+str(j)+') = {'+str(1000*int(z+1))+str(i+1)+str(j)+','+str(i+1)+str(j)+'};\n' )
                f.write('//+\n' )
                
    # Creation of the FRAP Lines
    if FRAP ==1:
        for i in range(0,int(len(list_points)-N-1),N):
            if sqrt((coords_afterFRAP_bis[list_points[i]][1] - coords_afterFRAP_bis[list_points[i+N]][1])*(coords_afterFRAP_bis[list_points[i]][1] - coords_afterFRAP_bis[list_points[i+N]][1]) + ((coords_afterFRAP_bis[list_points[i]][0] -  coords_afterFRAP_bis[list_points[i+N]][0])*(coords_afterFRAP_bis[list_points[i]][0] - coords_afterFRAP_bis[list_points[i+N]][0])))< d :

                f.write('Line(3000'+str(i+1)+') = {3000'+str(i)+','+'3000'+str(i+N)+'};\n' )
                f.write('//+\n' )

                f.write('Line(4000'+str(i+1)+') = {'+str(4000*int(z+1))+str(i)+','+str(4000*int(z+1))+str(i+N)+'};\n' )
                f.write('//+\n' )


                f.write('Line(5000'+str(i+1)+') = {'+str(4000*int(z+1))+str(i)+','+'3000'+str(i+N)+'};\n' )
                f.write('//+\n' )
                
    # Creation of the contours mesh
    f.write('Field[1] = Distance;\n' )
    f.write('//+\n' )
    f.write('Field[1].EdgesList = {' )
    
    for i in range(len(list_objects)):
        for j in range(len(list_objects[i])-1):
            if sqrt((coords_beforeFRAP_bis[list_objects[i][j]][1] - coords_beforeFRAP_bis[list_objects[i][j+1]][1])*(coords_beforeFRAP_bis[list_objects[i][j]][1] -coords_beforeFRAP_bis[list_objects[i][j+1]][1]) + ((coords_beforeFRAP_bis[list_objects[i][j]][0] -  coords_beforeFRAP_bis[list_objects[i][j+1]][0])*(coords_beforeFRAP_bis[list_objects[i][j]][0] - coords_beforeFRAP_bis[list_objects[i][j+1]][0])))< d :
                
                f.write(str(1000*int(z+1))+str(i+1)+str(j)+',')
                f.write(str(2000*int(z+1))+str(i+1)+str(j)+',')                
                f.write(str(i+1)+str(j)+',')
                a = i+1
                b= j
    f.write(str(a)+str(b))
    f.write('};\n' )
    f.write('//+\n' )
    f.write('Field[1].NNodesByEdge = 100;\n' )
    f.write('//+\n' )
    f.write('Field[2] = Threshold;\n' )
    f.write('//+\n' )
    f.write('Field[2].IField = 1;\n' )
    f.write('//+\n' )
    f.write('Field[2].LcMin = '+ str(lc_min)+';\n' )
    f.write('//+\n' )
    f.write('Field[2].LcMax = '+ str(lc_max)+';\n' )
    f.write('//+\n' )
    f.write('Field[2].DistMin = '+ str(d_min_mesh)+';\n' )
    f.write('//+\n' )
    f.write('Field[2].DistMax = '+ str(d_max_mesh)+';\n' )
    f.write('//+\n' ) 
    
    # Creation of the selected contours mesh
    f.write('Field[3] = Distance;\n' )
    f.write('//+\n' )
    f.write('Field[3].EdgesList = {' )
    
    for j in range(len(list_objects[int(choose)])-1):
        if sqrt((coords_beforeFRAP_bis[list_objects[int(choose)][j]][1] - coords_beforeFRAP_bis[list_objects[int(choose)][j+1]][1])*(coords_beforeFRAP_bis[list_objects[int(choose)][j]][1] -coords_beforeFRAP_bis[list_objects[int(choose)][j+1]][1]) + (coords_beforeFRAP_bis[list_objects[int(choose)][j]][0] -  coords_beforeFRAP_bis[list_objects[int(choose)][j+1]][0])*(coords_beforeFRAP_bis[list_objects[int(choose)][j]][0] - coords_beforeFRAP_bis[list_objects[int(choose)][j+1]][0]))< d :
            f.write(str(int(choose)+1)+str(j)+',')
            f.write(str(2000*int(z+1))+str(int(choose)+1)+str(j)+',')                
            f.write(str(1000*int(z+1))+str(int(choose)+1)+str(j)+',')
            a = int(choose)+1
            b= j
    f.write(str(a)+str(b))
    f.write('};\n' )
    f.write('//+\n' )
    f.write('Field[3].NNodesByEdge = 100;\n' )
    f.write('//+\n' )
    f.write('Field[4] = Threshold;\n' )
    f.write('//+\n' )
    f.write('Field[4].IField = 3;\n' )
    f.write('//+\n' )
    f.write('Field[4].LcMin = '+ str(lc_min_selected)+';\n' )
    f.write('//+\n' )
    f.write('Field[4].LcMax = '+ str(lc_max_selected)+';\n' )
    f.write('//+\n' )
    f.write('Field[4].DistMin = '+ str(d_min_mesh_selected)+';\n' )
    f.write('//+\n' )
    f.write('Field[4].DistMax = '+ str(d_max_mesh_selected)+';\n' )
    f.write('//+\n' )
    
    # Creation of the FRAP contours mesh
    if FRAP == 1:
        f.write('Field[5] = Distance;\n' )
        f.write('//+\n' )
        f.write('Field[5].EdgesList = {' )
        for i in range(0,int(len(list_points)-N-1),N):
            f.write('3000'+str(i+1)+',')
            f.write('4000'+str(i+1)+',')                
            f.write('5000'+str(i+1)+',')
            a = 1
            b= i
        f.write(str(a)+str(b))
        f.write('};\n' )
        f.write('//+\n' )
        f.write('Field[5].NNodesByEdge = 100;\n' )
        f.write('//+\n' )
        f.write('Field[6] = Threshold;\n' )
        f.write('//+\n' )
        f.write('Field[6].IField = 5;\n' )
        f.write('//+\n' )
        f.write('Field[6].LcMin = '+ str(lc_min_frap)+';\n' )
        f.write('//+\n' )
        f.write('Field[6].LcMax = '+ str(lc_max_frap)+';\n' )
        f.write('//+\n' )
        f.write('Field[6].DistMin = '+ str(d_min_frap)+';\n' )
        f.write('//+\n' )
        f.write('Field[6].DistMax = '+ str(d_max_frap)+';\n' )
        f.write('//+\n' )
    f.write('Field[7] = Min;\n' )
    f.write('//+\n' )
    if FRAP ==0:
        f.write('Field[7].FieldsList = {4,2};\n' )
        f.write('//+\n' )
        f.write('Background Field = 7;\n' )
        f.write('//+\n' )
        f.close()
    if FRAP ==1:
        f.write('Field[7].FieldsList = {4,2,6};\n' )
        f.write('//+\n' )
        f.write('Background Field = 7;\n' )
        f.write('//+\n' )
        f.close()