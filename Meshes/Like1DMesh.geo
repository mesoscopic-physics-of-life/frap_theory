

lc = 10;

//+
Point(1) = {0, 0, 0, lc/100};
//+
Point(2) = {0, 0.01, 0, lc/100};
//+
Point(3) = {30, 0.01, 0, lc};
//+
Point(4) = {30, 0, 0, lc};


//+
Point(5) = {1, 0.05, 0, 1};

//+
Point(6) = {1, 0, 0, 1};



//+
Line(1) = {2, 1};
//+
Line(2) = {2, 3};
//+
Line(3) = {4, 3};
//+
Line(4) = {1, 4};
//+
Line Loop(1) = {4, 3, -2, 1};
//+
Plane Surface(1) = {1};
//+
Physical Line("right") = {3};
//+
Physical Line("top", 5) = {2};
//+
Physical Line("bot", 6) = {4};
//+
Physical Line("Left", 7) = {1};
Physical Surface("Plane") = {1};

//+
Field[1] = Distance;
//+
Field[1].NodesList = {6,5};
Field[1].NNodesByEdge = 500;
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lc/1000;
Field[2].LcMax = lc/5;
Field[2].DistMin = 0.5;
Field[2].DistMax = 1;

Background Field = 2;
//+


