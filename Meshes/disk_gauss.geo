SetFactory("OpenCASCADE");
Disk(1) = {0, 0, 0, 4};

Physical Surface("surfacedomain") = {1};

Field[3] = MathEval;
Field[3].F = ".5 - (.5-0.01)*exp(-(1./0.1) *((sqrt(x*x+y*y)-1))*((sqrt(x*x+y*y)-1)))";

Mesh.Algorithm = 5; // This is necessary to avoid irregularities of Gaussian
Background Field = 3;
