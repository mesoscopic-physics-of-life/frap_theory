//+



SetFactory("OpenCASCADE");
Sphere(1) = {0, 0, 0, 30, -Pi/2, Pi/2, 2*Pi};
//+
lc = 20;
Point(3) = {0, 0, 0, 0.03};


Physical Surface("surfacedomain") = {1};
//+
Physical Volume("vilumedomain") = {1};
//+


// => Better precision but huge computing time:
//Field[3] = MathEval;
//Field[3].F = "20  - 19.997*exp(-(1./2) *((sqrt(x*x+y*y+z*z)-1)/0.15)*((sqrt(x*x+y*y+z*z)-1)/0.15))";
//+
//Field[7] = MathEval;
//Field[7].F = "20 - 19.99*exp(-(1./2) *((sqrt(x*x+y*y+z*z)-1)/1.5)*((sqrt(x*x+y*y+z*z)-1)/1.5))";


Field[3] = MathEval;
Field[3].F = "20  - 19.995*exp(-(1./2) *((sqrt(x*x+y*y+z*z)-1)/0.15)*((sqrt(x*x+y*y+z*z)-1)/0.15))";
//+
Field[7] = MathEval;
Field[7].F = "20 - 19.985*exp(-(1./2) *((sqrt(x*x+y*y+z*z)-1)/2)*((sqrt(x*x+y*y+z*z)-1)/2))";



Field[8] = Min;
Field[8].FieldsList = {7,3};



//+
Field[1] = Distance;
//+
Field[1].NodesList = {3};
Field[1].NNodesByEdge = 10;
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = 0.03;
Field[2].LcMax = 20;
Field[2].DistMin = 1.1;
Field[2].DistMax = 25;


Field[4] = Min;
Field[4].FieldsList = {2,8};

//+
Field[5] = Ball;
//+
Field[5].Radius = 1.05;
//+
Field[5].VIn = 0.00001;
//+
Field[5].VOut = 0.03;
Field[6] = Max;
Field[6].FieldsList = {5,4};
Background Field = 6;





