SetFactory("OpenCASCADE");
Mesh.Algorithm = 5; // Necessary to avoid problems with small lc at VIn
Disk(1) = {0, 0, 0, 4};

Field[5] = Ball;
Field[5].Radius = 0.5;
Field[5].VIn = 0.01;
Field[5].VOut = 0.05;
Background Field = 5;