// NOT TESTED, POTENTIAL ISSUES
SetFactory("OpenCASCADE");
Sphere(1) = {0, 0, 0, 0.8, -Pi/2, Pi/2, 2*Pi};
Sphere(2) = {0, 0, 0, 1., -Pi/2, Pi/2, 2*Pi};
Sphere(3) = {0, 0, 0, 2, -Pi/2, Pi/2, 2*Pi};
BooleanDifference(4) = { Volume{2}; Delete; }{ Volume{1};};

lcar1 = .35;
lcar2 = .15;
lcar3 = .01;
Physical Surface("surfacedomain") = {3};
Physical Volume("volumedomain") = {3};

Characteristic Length{ PointsOf{ Volume{4}; } } = lcar3;
Characteristic Length{ PointsOf{ Volume{1}; } } = lcar2;
Characteristic Length{ PointsOf{ Volume{3}; } } = lcar1;

Coherence;