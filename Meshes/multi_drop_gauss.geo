SetFactory("OpenCASCADE");
Box(1) = {0, 0, 0, 8, 8, 8};

Physical Surface("surfacedomain") = {1};
Physical Volume("volumedomain") = {1};

Field[3] = MathEval;
Field[3].F = ".4 - .39*exp(-(1./0.25) *((sqrt((x-4)*(x-4)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25))*((sqrt((x-4)*(x-4)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25)))";

Field[4] = MathEval;
Field[4].F = ".4 - .39*exp(-(1./0.05) *((sqrt((x-4.5)*(x-4.5)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25))*((sqrt((x-4.5)*(x-4.5)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25)))";

Field[5] = MathEval;
Field[5].F = ".4 - .39*exp(-(1./0.05) *((sqrt((x-3.5)*(x-3.5)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25))*((sqrt((x-3.5)*(x-3.5)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25)))";

Field[6] = MathEval;
Field[6].F = ".4 - .39*exp(-(1./0.05) *((sqrt((x-4)*(x-4)+(y-4.5)*(y-4.5)+(z-0.25)*(z-0.25))-0.25))*((sqrt((x-4)*(x-4)+(y-4.5)*(y-4.5)+(z-0.25)*(z-0.25))-0.25)))";

Field[7] = MathEval;
Field[7].F = ".4 - .39*exp(-(1./0.05) *((sqrt((x-4)*(x-4)+(y-3.5)*(y-3.5)+(z-0.25)*(z-0.25))-0.25))*((sqrt((x-4)*(x-4)+(y-3.5)*(y-3.5)+(z-0.25)*(z-0.25))-0.25)))";

Field[8] = Min;
Field[8].FieldsList = {3,4,5,6,7};

Mesh.Algorithm = 5;
Background Field = 8;