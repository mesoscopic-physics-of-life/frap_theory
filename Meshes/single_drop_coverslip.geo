SetFactory("OpenCASCADE");
Box(1) = {0, 0, 0, 8, 8, 8};

Physical Surface("surfacedomain") = {1};
Physical Volume("volumedomain") = {1};

Field[3] = MathEval;
Field[3].F = ".25 - .24*exp(-(1./0.25) *((sqrt((x-4)*(x-4)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25))*((sqrt((x-4)*(x-4)+(y-4)*(y-4)+(z-0.25)*(z-0.25))-0.25)))";

Field[8] = Min;
Field[8].FieldsList = {3};

Mesh.Algorithm = 1;
Background Field = 8;
