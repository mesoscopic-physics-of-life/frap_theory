SetFactory("OpenCASCADE");
Sphere(1) = {0, 0, 0, 4, -Pi/2, Pi/2, 2*Pi};

Physical Surface("surfacedomain") = {1};
Physical Volume("volumedomain") = {1};

Field[3] = MathEval;
Field[3].F = "1.0 - (1.0-0.0025)*exp(-(1./0.4) *((sqrt(x*x+y*y+z*z)-0.15))*((sqrt(x*x+y*y+z*z)-0.15)))";

Background Field = 3;