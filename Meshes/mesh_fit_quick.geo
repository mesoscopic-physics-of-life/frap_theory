//+



SetFactory("OpenCASCADE");
Sphere(1) = {0, 0, 0, 100, -Pi/2, Pi/2, 2*Pi};
//+
lc = 20;
Point(3) = {0, 0, 0, 0.1};


Physical Surface("surfacedomain") = {1};
//+
Physical Volume("vilumedomain") = {1};
//+



//+
Field[1] = Distance;
//+
Field[1].NodesList = {3};
Field[1].NNodesByEdge = 10;
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = 2;
Field[2].LcMax = 40;
Field[2].DistMin = 11;
Field[2].DistMax = 40;



Background Field = 2;

