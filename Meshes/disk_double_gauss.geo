SetFactory("OpenCASCADE");
Mesh.Algorithm = 5; // Necessary to avoid problems with small lc
Disk(1) = {0, 0, 0, 4};

Physical Surface("surfacedomain") = {1};

Field[3] = MathEval;
Field[3].F = ".35 - (.335)*exp(-(1./0.05) *((sqrt(x*x+y*y+z*z)-0.8))*((sqrt(x*x+y*y+z*z)-0.8)))";
Field[4] = MathEval;
Field[4].F = ".35 - (.335)*exp(-(1./0.05) *((sqrt(x*x+y*y+z*z)-1.1))*((sqrt(x*x+y*y+z*z)-1.1)))";

Field[5] = Min;
Field[5].FieldsList={3,4};
Background Field = 5;