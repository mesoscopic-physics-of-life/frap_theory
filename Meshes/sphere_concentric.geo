// NOT TESTED, POTENTIAL ISSUES
SetFactory("OpenCASCADE");
Sphere(1) = {0, 0, 0, 0.8, -Pi/2, Pi/2, 2*Pi};
Sphere(2) = {0, 0, 0, 0.95, -Pi/2, Pi/2, 2*Pi};
Sphere(3) = {0, 0, 0, 1.05, -Pi/2, Pi/2, 2*Pi};
Sphere(4) = {0, 0, 0, 2, -Pi/2, Pi/2, 2*Pi};

lcar1 = .45;
lcar2 = .35;
lcar3 = .04;
Physical Surface("surfacedomain") = {4};
//+
Physical Volume("volumedomain") = {1,2,3,4};
//+
Characteristic Length{ PointsOf{ Volume{1}; } } = lcar1;
Characteristic Length{ PointsOf{ Volume{2}; } } = lcar3;
Characteristic Length{ PointsOf{ Volume{3}; } } = lcar3;
Characteristic Length{ PointsOf{ Volume{4}; } } = lcar1;


Coherence; //Necessary to connect all volumes/make mesh coherent