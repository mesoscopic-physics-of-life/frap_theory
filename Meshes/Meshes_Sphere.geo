//+



SetFactory("OpenCASCADE");
Sphere(1) = {0, 0, 0, 1, -Pi/2, Pi/2, 2*Pi};
//+
Point(3) = {0, 0, 0, 0.015};
lc = .15;

Physical Surface("surfacedomain") = {1};
//+
Physical Volume("vilumedomain") = {1};
//+


Field[1] = Distance;
//+
Field[1].NodesList = {3};
Field[1].NNodesByEdge = 20;
Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lc / 10;
Field[2].LcMax = lc;
Field[2].DistMin = 0.3;
Field[2].DistMax = 0.5;

Field[3] = Min;
Field[3].FieldsList = {2};
Background Field = 3;
