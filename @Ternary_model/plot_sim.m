function plot_sim(T, mode, nth, color, start, x_scale)
%PLOT_SIM Show movie of simulation
% mode 'mov' ... show movie
% mode 'plot' ... show plot

if nargin < 6; x_scale = 1; end

si = size(T.sol);
if nargin < 3
    nth = 1;
    color = 'red';
    start = 1;
end
if strcmp(mode, 'mov')
    figure(20); hold on;
    for i = 1:nth:length(T.t)
        cla;
        axis([0, T.system_size, 0, max(T.sol(:))]);
        xlim([0, 10]);
        ax = gca; ax.FontSize = 12;
        xlabel('position'); ylabel('probability [not normalized]');
        plot(T.x, T.phi_tot(T.x, T.a, T.b, T.e, T.u0, T.t(i)*T.v),...
            'LineWidth', 2, 'LineStyle', '--');
        if length(size(T.sol))==2
            plot(T.x, T.sol(i, :), 'LineWidth', 2);
        elseif length(size(T.sol))==3
            plot(T.x, T.sol(i, :, 1), 'LineWidth', 2, 'Color', color);
            plot(T.x, T.sol(i, :, 2), 'LineWidth', 2, 'Color', 'g');
            plot(T.x, T.sol(i, :, 1) + T.sol(i, :, 2),  'k');
            legend('Droplet scaffold', 'Substrate', 'Cleaved product');
        end
        text(abs(-T.a+2), 0.7, num2str(T.t(i)));
        shg;
        pause();
%         print([num2str(i),'.png'],'-dpng')
    end
elseif strcmp(mode, 'plot')
    figure(20); hold on;
    xlim([-inf, -2*T.a]); %ylim([-inf, max(T.sol(:))]);
    ax = gca;
    ax.FontSize = 12;
    xlabel('x [\mum]'); ylabel('volume fraction'); 
%     N = max(max(T.sol([1, 2:nth:si(1)], :)));
    N = 1;
    plot(T.x*x_scale, Ternary_model.phi_tot(T.x, T.a, T.b, T.e, T.u0, 0),...
        'LineWidth', 1, 'LineStyle', '--', 'Color', [0.97, 0.55, 0.03]);
    if length(size(T.sol))==2
        plot(T.x*x_scale, T.sol([1, start:nth:si(1)], :)/N, 'LineWidth', 1,...
            'Color', color);
    elseif length(size(T.sol))==3
        plot(T.x*x_scale, T.sol([1, start:nth:si(1)], :, 1)/N, 'LineWidth', 1,...
            'Color', color);
        plot(T.x*x_scale, T.sol([1, start:nth:si(1)], :, 2)/N, 'LineWidth', 1,...
            'Color', 'g');
        plot(T.x, T.sol([1, start:nth:si(1)], :, 1) +...
             T.sol([1, start:nth:si(1)], :, 2),  'k', 'LineWidth', 2);
    end
end
end

