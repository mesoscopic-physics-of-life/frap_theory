function p = phi_tot(x, a, b, e, u0, vt)
    p = e*tanh(-(x+a+vt)/b)+u0;
%     p = e*tanh(-(x+a+vt)/b)+u0-0.23*exp(-(1/2*(x+a)/b).^2);
end