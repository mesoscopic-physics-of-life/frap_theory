function create_mesh(T)
%CREATE_MESH creates mesh for solving Ternary_model.
%   Mesh density larger for steeper gradients.

if strcmp(T.mode, 'Constituent') || strcmp(T.mode, 'Const_mob') || T.v == 0
% Start with left side of mesh, at r=0/x=0, with a given step size
    x = linspace(0, -0.75*T.a, 200);
    x = [x, x(end)+0.001/T.precision];
    while x(end)<-T.a
        x_temp = x(end)-x(end-1);
        phi_nm1 = T.phi_tot(x(end), T.a, T.b, T.e, T.u0, 0);
        phi_n = T.phi_tot(x(end)+x_temp, T.a, T.b, T.e, T.u0, 0);
        while phi_nm1-phi_n > 0.002/T.precision
            x_temp = x_temp/2;
            phi_n = T.phi_tot(x(end)+x_temp, T.a, T.b, T.e, T.u0, 0);
        end
        x = [x, x(end)+x_temp];
    end
    x = [x, x(end)+cumsum(fliplr(diff(x)))];
    x_middle = linspace(-2*T.a+x(end)-x(end-1), -10*T.a, 100*T.precision);
    x_right = linspace(-10*T.a+0.1, T.system_size, 100*T.precision);
    T.x = [x, x_middle, x_right];
    T.x = T.x(1:find(T.x-T.system_size>=0, 1)-1);

    % Fine mesh close to x0 if ic = 'Gauss'
    [~, ind] = min(abs(T.x-T.x0));
    if (T.x(ind+20)-T.x(ind) > 0.1) && strcmp(T.ic, 'Gauss')
        T.x = unique(sort([T.x, linspace(T.x0-0.099, T.x0+0.099, 200)]));
    end
elseif strcmp(T.mode, 'Client') && T.v ~= 0
%     T.x = linspace(0, T.system_size, 24000);
    T.x = [linspace(0, -T.a-1, 1000), linspace(-T.a-1+0.0001, -T.a+1, 4800),...
           linspace(-T.a+1+0.0001, T.system_size, 1000)];
    % Fine mesh close to x0 if ic = 'Gauss'
    [~, ind] = min(abs(T.x-T.x0));
    [~, ind_plus] = min(abs(T.x-(T.x0+0.6)));
    [~, ind_minus] = min(abs(T.x-(T.x0-0.6)));
    T.x = [T.x(1:ind_minus),...
           linspace(T.x(ind_minus)+0.0001, T.x(ind_plus)-0.0001, 1000),...
           T.x(ind_plus:end)];
end
end