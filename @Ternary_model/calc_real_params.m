function calc_real_params(T)
% Calculate important real-life parameters, such as D_in, D_out,
% partitioning etc..
T.real_params.phi_in = Ternary_model.phi_tot(-1000, T.a, T.b, T.e, T.u0, 0);
T.real_params.phi_out = Ternary_model.phi_tot(1000, T.a, T.b, T.e, T.u0, 0);
T.real_params.partitioning = T.real_params.phi_in/T.real_params.phi_out;

if strcmp(T.mode, 'Constituent') || strcmp(T.mode, 'Client')
T.real_params.Gamma_in = Ternary_model.gamma0(-1000, T.a, T.b, T.e_g0,...
                                               T.u_g0, 0, T.e, T.u0);
T.real_params.Gamma_out = Ternary_model.gamma0(1000, T.a, T.b, T.e_g0,...
                                               T.u_g0, 0, T.e, T.u0);
T.real_params.D_in = (1-T.real_params.phi_in)*T.real_params.Gamma_in;
T.real_params.D_out = (1-T.real_params.phi_out)*T.real_params.Gamma_out;

elseif strcmp(T.mode, 'Const_mob')
    T.real_params.D_in = (1-T.real_params.phi_in+...
                            T.e_g0*T.real_params.phi_in^2);
    T.real_params.D_out = (1-T.real_params.phi_out+...
                            T.e_g0*T.real_params.phi_out^2);
elseif strcmp(T.mode, 'Mobility_ratio')
    % e_g0 and u_g0 in this case have nothing to do with tanh. See
    % flory_hugg_pde
    T.real_params.D_in = T.u_g0.*(1-T.e_g0*T.real_params.phi_in);
    T.real_params.D_out = T.u_g0.*(1-T.e_g0*T.real_params.phi_out);
end
end