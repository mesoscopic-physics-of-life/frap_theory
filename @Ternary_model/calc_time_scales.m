function calc_time_scales(T)
%CALC_TIME_SCALES calculates inside and outside timescales based on
%partitioning factor and diffusion coefficients, as well as droplet radius

p = T.e/T.u0 + 1; %Partitioning, +1, due to T.e being difference, not total
T.tau.R_out = p^(1/3)*abs(T.a);
T.tau.D_in = Ternary_model.gamma0(T.x(1), T.a, T.b, T.e_g0, T.o_g0)*(1-T.u0-T.e);
T.tau.D_out = Ternary_model.gamma0(T.x(end), T.a, T.b, T.e_g0, T.o_g0)*(1-T.u0);
T.tau.tau_in = T.a^2/T.tau.D_in;
T.tau.tau_out = T.tau.R_out^2/T.tau.D_out;