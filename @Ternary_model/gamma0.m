function g0 = gamma0(x, a, b, e_g0, u_g0, vt, e, u0)
%GAMMA0 calculate spatially dependent mobility
%   Based on a tanh profile.
if isnumeric(e_g0)
    g0 = e_g0*(tanh((x+a+vt)/b)+1)/2+u_g0;
elseif strcmp(e_g0, 'const')
    g0 = u_g0./(1-Ternary_model.phi_tot(x, a, b, e, u0, vt));
elseif ~isempty(str2double(e_g0))
    eg = str2double(e_g0);
    al = 2.7;
    p1 = eg*(tanh((x+a+vt)/b)+1)/2+u_g0;
    p2 = eg/2/b*(1-tanh((x+a+vt)/b).^2)/al;
    g0 = p1-p2;
%     figure; hold on;
%     plot(x, p1); plot(x, p2); plot(x, g0);
%     pause();
%     close all;
end
end