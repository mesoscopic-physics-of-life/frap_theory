function grad = gradient_analytical(x, a, b, e, vt)
    grad = -e*(1-tanh(-(x+a+vt)/b).^2)/b;
%     grad = -e*(1-tanh(-(x+a+vt)/b).^2)/b+0.23*exp(-(1/2*(x+a)/b).^2)*(x+a)/(2*b^2);
end