classdef Ternary_model < handle
    % TERNARY_MODEL Numerical solution of ternary FRAP model with solvent, 
    % bleached and unbleached species. Model is assumed to be equilibrated
    % (bleached+unbleached=const.=pt). Then bleached species initial
    % conditions are introduced. Integration of model via pdepe.

    properties
        pa = '~/Desktop/DropletFRAP/Latex/Figures/Fig2/';
        geometry = 2; % 2 ... Radial, 1 ... Cylindrical, 0 ... Slab
        ic %'FRAP', 'Gauss', 'Uniform'
        mode % 'Constituent': LLPS molecules move. 'Client': client moves
        precision = 5;
        sol
        t
        tau
        x
        ga0
        phi_t
        % Parameters
        a = -1;
        b = 0.025;
        u0 = 0.05;
        e = 0.4;
        e_g0 = 0.16; % mobility spread. Also used in square mobility ansatz.
        ic_c % initial concentration inside droplet
        j % parameter characterizing the flux in F.-P. eq.. Overloaded for BC.
        u_g0 = 0.2;
        system_size = 300;
        x0 = 1; % Center of Gauss initial condition
        real_params;
        v; % velocity of boundary
        par_n; % parameters for species n_1, ...n_n
    end
    methods
        function T = Ternary_model(geometry, ic, params, t, prec, par_n)
            T.geometry = geometry;
            T.ic = ic;
            T.precision = prec;
            T.t = t;
            if ~isempty(params)
                T.a = params{1};
                T.b = params{2};
                T.u0 = params{3};
                T.e = params{4};
                T.e_g0 = params{5};
                T.u_g0 = params{6};
                T.system_size = params{7};
                T.x0 = params{8};
                T.v = params{9};
                T.j = params{10};
                T.mode = params{11};
                T.ic_c = params{12};
            end
            if nargin == 6; T.par_n = par_n; end
            T.create_mesh();
            T.phi_t = Ternary_model.phi_tot(T.x, T.a, T.b, T.e, T.u0, 0);
            T.ga0 = Ternary_model.gamma0(T.x, T.a, T.b, T.e_g0, T.u_g0,...
                                         0, T.e, T.u0);
            T.calc_real_params;
        end
        create_mesh(T)
        plot_sim(T, mode, nth, color, N, i)
        solve_tern_frap(T)
        calc_time_scales(T);
        calc_real_params(T);
    end
    methods(Static)
        pt = phi_tot(x, a, b, e, u0, vt)
        g = gradient_analytical(x, a, b, e, vt)
        g0 = gamma0(x, a, b, e_g0, u_g0, vt, e, u0)
    end
end