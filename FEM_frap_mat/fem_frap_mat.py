from celluloid import Camera
from dolfin import *
from fenics import *
from mshr import *
from math import *
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
from numpy import trapz
from scipy.interpolate import interp1d

import numpy as np
import matplotlib.animation as animation
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import time


def Form_expression(Phi_u,Phi_u0,Gamma_0,Phi_tot,dt,v):
    '''
    This function return the weak form of the PDE which is solve
    '''
    Form = (inner((Phi_u-Phi_u0)/dt, v) - inner(Gamma_0*((1-Phi_tot)/Phi_tot)*
            Phi_u*grad(Phi_tot) -Gamma_0*(1-Phi_tot)*grad(Phi_u), grad(v))) *dx
    return Form

def def_fenics_space(File_mesh):
    
    mesh = Mesh(File_mesh)
    P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
    V = FunctionSpace(mesh,P1)
    v = TestFunction(V) 
    Phi_u = Function(V)
    
    return V, v, Phi_u 

def def_problem(dimension, parameters, File_mesh, IC):
    '''
    Create the problem on Fenics from an imported mesh.
    '''
    
    V,v, Phi_u =  def_fenics_space(File_mesh)

    Phi_tot,Phi_u0,Gamma_0 = initial_condition(dimension, parameters, V, IC)

    dt = parameters[7]
    Form = Form_expression(Phi_u,Phi_u0,Gamma_0,Phi_tot,dt,v)

    return Form,Phi_u0, Phi_u

def initial_condition(dimension, parameters, space, IC):
    '''
     Initialize the initial conditions in function of the dimension 
     and of the the IC (IC = 0: Full frap, IC = 1: Half frap)
    '''
    a = parameters[0] 
    b = parameters[1] 
    e = parameters[2] 
    u0 = parameters[3]
    e_g0 = parameters[4] 
    u_g0 = parameters[5] 
    
    if dimension == 3:
        Phi_tot = interpolate(Expression('e*tanh(-(sqrt(x[0]*x[0]+x[1]*x[1]+'+
                                         'x[2]*x[2])+a)/b)+u0',degree=1,e = e,
                                          a = a, b = b, u0 = u0),space)
        Gamma_0 = interpolate(Expression(
                'e_g0*(tanh((sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])+a)/b)+1)/2+u_g0;',
                                           degree = 1, e_g0 = e_g0, a = a, b = b,
                                                            u_g0 = u_g0), space) 
        if IC == 0:
            Phi_u0 = interpolate(Expression('sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2])'+
                                            ' <= -a ? 0.0: (u0 - e)', degree=1, 
                                             a = a, e = e, u0 = u0),space)
        if IC == 1:
            Phi_u0 = interpolate(Expression(
                'sqrt(x[1]*x[1]+x[0]*x[0]+x[2]*x[2]) <= -a && atan2(x[1],x[0])'+
                ' < 0  ? 0.0: sqrt(x[1]*x[1]+x[0]*x[0]+x[2]*x[2]) <= -a && '+
                'atan2(x[1],x[0]) >= 0 ?  (u0+e): (u0 - e)',degree=1, a = a,
                 e = e, u0 = u0),space)
        return Phi_tot,Phi_u0,Gamma_0
        
    if dimension == 2:
        Phi_tot = interpolate(Expression('e*tanh(-(sqrt(x[0]*x[0]+x[1]*x[1]'+
                                         ')+a)/b)+u0', degree=1,e = e, a = a,
                                          b = b, u0 = u0),space)
        Gamma_0 = interpolate(Expression('e_g0*(tanh((sqrt(x[0]*x[0]+x[1]*x[1]'+
                                         ')+a)/b)+1)/2+u_g0;',degree = 1,
                                         e_g0 = e_g0, a = a, b = b,
                                         u_g0 = u_g0), space)  
        if IC == 0:
            Phi_u0 = interpolate(Expression('sqrt(x[0]*x[0]+x[1]*x[1]) <= -a ?'+
                                            ' 0.0: (u0 - e)',degree=1, a = a,
                                             e = e, u0 = u0),space)
        if IC == 1:
            Phi_u0 = interpolate(Expression('sqrt(x[1]*x[1]+x[0]*x[0]) <= -a '+
                                    '&& atan2(x[1],x[0]) < 0 ? 0.0: '+
                                    'sqrt(x[1]*x[1]+x[0]*x[0]) <= -a '+
                                    '&& atan2(x[1],x[0]) >= 0 ?  (u0+e): (u0 - e)',
                                    degree=1, a = a, e = e, u0 = u0),space)
        return Phi_tot,Phi_u0,Gamma_0
    
    if dimension == 1:           
        Phi_tot = interpolate(Expression(
                'e*tanh(-(x[0]+a)/b)+u0', degree=1,e = e, a = a, b = b, u0 = u0),
                              space)
        Gamma_0 = interpolate(Expression('e_g0*(tanh((x[0]+a)/b)+1)/2+u_g0;',
                                         degree = 1,e_g0 = e_g0, a = a, b = b,
                                         u_g0 = u_g0), space)
        Phi_u0 = interpolate(Expression('x[0] <= -a ? 0.0: (u0 - e)' ,
                                            degree=1, a = a, e = e, u0 = u0),
                                            space)
        return Phi_tot,Phi_u0,Gamma_0                  
                                 
                                 
def save_result(array_result, File_save, Computing_time, 
                File_save_computing_time, RootMeanSquare, File_save_RMS ):
    '''
    Save the results in .txt file
    '''
    np.savetxt(File_save, array_result[:,:], delimiter = ',')
    np.savetxt(File_save_RMS, RootMeanSquare, delimiter = ',')
    np.savetxt(File_save_computing_time, Computing_time, delimiter = ',')

def import_matlab():
    '''
    Import the matlab results, this function delete the point 
    in double in order to be able to interpolated the matlab results
    '''
    Matlab_result_import = np.genfromtxt('resultsmatlab.txt', delimiter=',')
    Matlab_mesh = np.genfromtxt('meshematlab.txt', delimiter=',')
    Matlab_phi_t = np.genfromtxt('phi_tot.txt', delimiter=',')
    ### Erase double point from matlab mesh ###
    u, indices = np.unique(Matlab_mesh, return_index = True)
    Matlab_mesh = np.take(Matlab_mesh, indices)
    Matlab_phi_t = np.take(Matlab_phi_t, indices)
    Matlab_result = np.take(Matlab_result_import[0, :], indices)
    for i in range (1, Matlab_result_import.shape[0]):
        Matlab_result = np.vstack((Matlab_result,
                        np.take(Matlab_result_import[int(i), :], indices)))
    return Matlab_result, Matlab_phi_t,Matlab_mesh
    
def mean_Phi_u(dimension, Phi_u0, IC):
    '''
    Calculate the mean of Phi_u in function of the dimension and of 
    the initial conditions,if half frap (IC == 1), we don't make mean 
    but just return the Phi_u along a line
    '''
    tol = .0001  # avoid hitting points outside the domain
    x = np.linspace(0 + tol, 5 - tol, 50000)
    PHI_U0 = []
    
    if dimension == 3:
        if IC == 1 :
            x = np.linspace(-5 + tol, 5 - tol, 50000)
            points1 = [(0, x_, 0) for x_ in x]
            Phi_u0_ = np.array([Phi_u0(point) for point in points1])
            return Phi_u0_
        points = [[(x_, 0, 0) for x_ in x],[(0, x_, 0) for x_ in x],
                  [(0, 0, x_) for x_ in x],
                  [(-x_, 0, 0) for x_ in x],[(0, -x_, 0) for x_ in x],
                  [(0, 0, -x_) for x_ in x]]
        for i in range(len(points)):
            PHI_U0.append(np.array([Phi_u0(pt)/6 for pt in points[i]]))
        Phi_u0_ = np.add(PHI_U0[0],PHI_U0[1])
        for j in range(2,6):
            Phi_u0_ =  np.add(Phi_u0_,PHI_U0[j])
        return Phi_u0_
    
    if dimension == 2:
        if IC == 1: 
            x = np.linspace(-5 + tol, 5 - tol, 50000)
            points1 = [(0, x_) for x_ in x]
            Phi_u0_ = np.array([Phi_u0(point) for point in points1])
            return Phi_u0_
        points = [[(x_, 0) for x_ in x],[(0, x_) for x_ in x],
                  [(-x_, 0) for x_ in x],
                  [(0, -x_) for x_ in x]]
        for i in range(len(points)):
            PHI_U0.append(np.array([Phi_u0(pt)/4 for pt in points[i]]))
        Phi_u0_ = np.add(PHI_U0[0],PHI_U0[1])
        Phi_u0_ = np.add(Phi_u0_,PHI_U0[2])
        Phi_u0_ = np.add(Phi_u0_,PHI_U0[3])
        return Phi_u0_
        
    if dimension == 1:
        if IC == 1: 
            points1 = [(x_, 0) for x_ in x]
            Phi_u0_ = np.array([Phi_u0(point) for point in points1])
            return Phi_u0_
        points1 = [(x_, 0) for x_ in x] 
        Phi_u0_ = np.array([Phi_u0(point) for point in points1])
        return Phi_u0_

def root_mean_square(RootMeanSquare, Phi_u0_, Matlab_mesh, Matlab_result, i):
    '''
    Calculate the Root mean square inside the Droplet (of radius 1)
    '''
    temp = 0
    tol = .0001  # avoid hitting points outside the domain
    x = np.linspace(0 + tol, 5 - tol, 50000)
    f = interp1d(x, Phi_u0_, kind='cubic')
    f2 = interp1d(Matlab_mesh, Matlab_result[int(i), :], kind='cubic')
    if i == 0:
        RootMeanSquare = []
    for i in range(10, 1001):
        temp = (f(i/1000)-f2(i/1000))*(f(i/1000)-f2(i/1000)) + temp
    temp = sqrt(temp/990)
    RootMeanSquare = np.append(RootMeanSquare,temp)
    return RootMeanSquare

def stock_result(Phi_u0_, IC):
    '''
    Allow to create the array in order to save the result
    '''
    L = []
    tol = .0001  # avoid hitting points outside the domain
    if IC == 0:
        x = np.linspace(0 + tol, 5 - tol, 50000)
        f = interp1d(x,Phi_u0_,kind='cubic')
        for j in range(1,2200):
            L.append(f(j/2000))
            
    if IC == 1:
        x = np.linspace(-5 + tol, 5 - tol, 50000)
        f = interp1d(x,Phi_u0_,kind='cubic')
        for j in range(1,1100):
            L.append(f((-1100+j)/1000))
        for j in range(0,1100):
            L.append(f(j/1000))
    return L

def solve_python(IC, Matlab, Plot, Save,Save_Paraview, dimension, parameters,
                 File_mesh,File_save, File_save_computing_time, File_save_RMS,
                 File_cFile):
    '''
     This function compute the Python results.
     IC: choose initial conditions: 0 = full frap, 1 = half frap
     Plot: 0 (no plot)or 1(activate plot)
     Save: 0 (no save)or 1(activate save)
     Save_Paraview: 0 (no save)or 1(activate save)
     dimension: Choose dimension of the problem, 1: like 1D, 2 = Disk, 3 = Sphere
     parameters:List of parameters: [a, b, e, u0, e_g0, u_g0, Nb_step, dt]
     File_mesh: File of the mesh
     File_save: File to the save result of python (along a line)
     File_save_computing_time: File to save the computing time
     File_cFile: File to save the xmdf file
    '''
    dt = parameters[7]
    RootMeanSquare = []
    List_Result = []
    Computing_Time = []
    if Matlab ==1:
        Matlab_result, Matlab_phi_t, Matlab_mesh = import_matlab()
    Form, Phi_u0, Phi_u = def_problem(dimension, parameters, File_mesh, IC)
    t = 0
    if Save_Paraview == 1:
        cFile = XDMFFile(File_cFile)
        cFile.write(Phi_u0, t)
    ti = time.time()
    for i in range(parameters[6]): 
        Phi_u0_ = mean_Phi_u(dimension, Phi_u0, IC)
        if Plot == 1:
            plot_Phi_u(Phi_u0_,i,IC,Matlab)
        if Save == 1:
            if i == 0:
                List_Result = stock_result(Phi_u0_, IC)
            else:
                List_Result = np.vstack((List_Result, stock_result(Phi_u0_, IC)))
            if Matlab ==1 :
                RootMeanSquare = root_mean_square(RootMeanSquare, Phi_u0_,
                                              Matlab_mesh, Matlab_result, i)     
        if dimension == 1 :
            solve(Form == 0, Phi_u)
        else :
            solve(Form == 0, Phi_u,
                  solver_parameters={'newton_solver': {'linear_solver': 'gmres'}})
        assign(Phi_u0, Phi_u)
        t += dt
        if Save_Paraview == 1:
            cFile.write(Phi_u0, t)
        if Plot == 1:
            print(time.time() - ti)
    if Save_Paraview == 1:
        cFile.close()
    if Save == 1:
        Computing_Time.append(time.time() - ti)
        np.savetxt(File_save, List_Result[:,:], delimiter = ',')
        np.savetxt(File_save_computing_time, Computing_Time, delimiter = ',')
        if Matlab == 1:
            np.savetxt(File_save_RMS, RootMeanSquare, delimiter = ',')

def plot_function(X,Y,legend,x_label, y_label,axis,pourcentage,ylog,
                  point,multiple):
    if pourcentage==1:
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        if point ==1:
            plt.plot(X,Y,'bo',label = legend)
        else:
            plt.plot(X,Y,label = legend)
        if ylog ==1:
            ax.set_yscale('log')
        ax.yaxis.set_major_formatter(ticker.PercentFormatter())
        plt.ylabel(y_label)
        plt.xlabel(x_label)
        if legend != 0:
            plt.legend(loc='best')
        plt.rc('xtick',labelsize=12)
        plt.rc('ytick',labelsize=10)
    else:
        if point ==1:
            plt.plot( X,Y,'bo', label = legend)
        else:
            plt.plot( X,Y, label = legend)
        if legend != 0:
            plt.legend(loc='best')
        if axis != 0:
            plt.axis(axis)
        if ylog ==1:
            plt.yscale('log')
        plt.ylabel(y_label)
        plt.xlabel(x_label) 
    if multiple == 0:
        plt.show()   

def plot_Phi_u(Phi_u0_, i, IC,Matlab):
    '''
    Plot the Phi_u. IC(Ic = 1: half frap, IC = 0: full frap),
    need to be in adequation with the results in order to have the right plot 
    '''
    tol = .0001  # avoid hitting points outside the domain

    if IC == 0:
        x = np.linspace(0 + tol, 5 - tol, 50000)
        xnew = np.linspace(0.01, 1.2, 5000)
        if Matlab ==1:
            Matlab_result,Matlab_phi_t,Matlab_mesh = import_matlab()
            f2 = interp1d(Matlab_mesh,Matlab_result[int(i), :],kind='cubic')
            plot_function(xnew,f2(xnew),'Matlab','r',r'$\phi_{u}$',0,0,0,0,1)
            plot_function(Matlab_mesh,Matlab_phi_t,
                    r'$\phi_{tot}$','r',r'$\phi_{u}$',[0,1.2,0.0,1],0,0,0,1)
        f = interp1d(x,Phi_u0_,kind='cubic')
        plot_function(xnew,f(xnew),'Python','r', 
                      r'$\phi_{u}$',[0,1.2,0.0,1],0,0,0,1)
       
    if IC ==1:
        x = np.linspace(-5 + tol, 5 - tol, 50000)
        f = interp1d(x,Phi_u0_,kind='cubic')
        xnew = np.linspace(-1.2, 1.2, 5000)
        plot_function(xnew,f(xnew),'Python','r',
                      r'$\phi_{u}$',[-1.2,1.2,0.0,1],0,0,0,1)
    plt.show()

def video(IC, video_save, *arg):
    '''
    Make a video from the result from the save file containing 
    the python results along a line, 
    the parameters IC need to be in adequation with the results 
    in order to have the right plot
    The lenght of the result need to be the same
    '''
    fig = plt.figure()
    camera = Camera(fig)
    cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
    L =[]
    i = 1
    for n in arg:
        temp = np.genfromtxt(n, delimiter=',')
        L.append(temp)
    X = []
    L0 = L[0]
    if IC == 0:
        for j in range(1,2200):
            X.append(j/2000)
        Matlab_result, Matlab_phi_t, Matlab_mesh = import_matlab()
        f1 = interp1d(Matlab_mesh, Matlab_phi_t, kind='cubic')
        for i in range(L0.shape[0]):
            f2 = interp1d(Matlab_mesh, Matlab_result[int(i), :], kind='cubic')
            plt.plot(X, f2(X), 'm', label= 'Matlab step %d' %(i+1))
            plt.plot(X, f1(X), 'k', label = r'$\phi_{tot}$')
            plt.xlabel('r')
            plt.ylabel(r'$\phi_{u}$')
            for j in range(len(L)):
                LISTE = L[j]
                plt.plot(X, LISTE[i,:],color=cycle[j],label ='Python %d'%(j+1))
            i += 1
            camera.snap()
    if IC == 1:
        for j in range(1,1100):
            X.append((-1100 + j)/1000)
        for j in range(0,1100):
            X.append(j/1000)
        for i in range(L0.shape[0]):
            for j in range(len(L)):
                LISTE = L[j]
                plt.plot(X, LISTE[i,:], color=cycle[j],label='Python %d'%(j+1))
            plt.axis([-1.2,1.2,0.0,1])
            plt.ylabel(r'$\phi_{u}$')
            plt.xlabel('r')  
            camera.snap()
    animation = camera.animate()
    animation.save(video_save)

def plot_save_result(IC, Matlab, *arg):
    '''
    Plot the result from the save file containing the python results along a line, 
    the parameters IC need to be in adequation with the results in order 
    to have the right plot
    Matlab =1: plot Matlab, Matlab =0: Plot only python
    The lenght of the result need to be the same
    '''
    L =[]
    for n in arg:
        temp = np.genfromtxt(n, delimiter=',')
        L.append(temp)
    X = []
    L0 = L[0]
    if IC == 0:
        for j in range(1,2200):
            X.append(j/2000)
        if Matlab ==1:
            Matlab_result, Matlab_phi_t, Matlab_mesh = import_matlab()
            f1 = interp1d(Matlab_mesh, Matlab_phi_t, kind='cubic')
        for i in range(L0.shape[0]):
            if Matlab ==1:
                f2 = interp1d(Matlab_mesh, Matlab_result[int(i), :],
                              kind='cubic')
                plot_function(X,f2(X), 'Matlab', r'$r$', 
                              r'$\phi_{u}$',0,0,0,0,1)
                plot_function(X,f1(X), r'$\phi_{tot}$', 
                              r'$r$', r'$\phi_{u}$',0,0,0,0,1)
            for j in range(len(L)):
                LISTE = L[j]
                plot_function(X,LISTE[i,:], 'FEM', r'$r$', r'$\phi_{u}$',0,0,0,0,1)
            plt.show()
    if IC == 1:
        for j in range(1,1100):
            X.append((-1100 + j)/1000)
        for j in range(0,1100):
            X.append(j/1000)
        for i in range(L0.shape[0]):
            for j in range(len(L)):
                LISTE = L[j]
                plot_function(X,LISTE[i,:], 'FEM', r'$r$', r'$\phi_{u}$',
                              [-1.2,1.2,0.0,1],0,0,0,1)
            plt.show()     

def plot_error_and_comp_time(relat_or_RMS,*arg):
    '''
    relat_or_RMS = 1 => relative error plot
    relat_or_RMS = 2 => RMS plot
    Plot the Root Mean square results in function of time and 
    the mean of this plot with the computing time
    Take in input RMS.txt files and Computingtime.txt files.
    It necessary to put in order the input:
    (RMS.txt, Computingtime.txt, RMS2.txt, Computingtime2.txt,...)
    and to have the same number of RMS.txt and Computingtime.txt
    '''
    RMS = []
    CT = []
    i = 0
    for n in arg:
        if i%2 ==0:
            temp = np.genfromtxt(n, delimiter=',')
            RMS.append(temp)
        else:
            temp = np.genfromtxt(n, delimiter=',')
            CT.append(temp)
        i+=1
    RMS0 = RMS[0]
    X = []
    X_RMS = np.linspace(0, len(RMS0),len(RMS0))
    for j in range(int(i/2)):
        plt.xlabel('Step')
        if relat_or_RMS == 2:
            plt.plot(X_RMS,RMS[j], label = 'Curve %d' %(j+1))
            plt.ylabel('Root Mean Square')
        if relat_or_RMS == 1:
#             RMS = [k*100 for k in RMS]
            plt.plot(X_RMS,RMS[j], label = 'Curve %d' %(j+1))
            plt.ylabel('Relative error(%)')
    plt.legend()
    plt.show()
    i = 0
    Mean = []    
    temp = []
    for jj in range(len(RMS)):
        temp = RMS[jj]
        m = 0
        for i in range(len(temp)):
            m = m + temp[i]
            m = m/len(temp)
        if relat_or_RMS == 1:
            Mean.append(m)
        if relat_or_RMS == 2:
            Mean.append(m)
            
    if relat_or_RMS == 2:     
        plot_function(CT,Mean,'RMS','Computing Time(s)', 
                      'Mean root mean square',0,0,1,1,0)
    if relat_or_RMS == 1:
        plot_function(CT,Mean,'error','Computing Time(s)', 
                      'Mean Relative Error',0,1,1,1,0)
        
def relativeerror(fileresult,FILESAVE):
    '''
    Compute the relative error between the matlab reference and the simulation.
    Fileresult is the file containing the results. 
    It should be create with the save results function
    FILESAVE is the file in which the relative error will be save
    '''
    AREA = []
    AX = []
    Matlab_result, Matlab_phi_t, Matlab_mesh = import_matlab()
    L0 = np.genfromtxt(fileresult, delimiter=',')
    ### Avoid to have very high value value with the ratio (divide by zero)
    for j in range(L0.shape[0]):
        for jj in range(L0.shape[1]):
            L0[j][jj] = L0[j][jj] + 0.01
            
    for j in range(Matlab_result.shape[0]):
        for jj in range(Matlab_result.shape[1]):
            Matlab_result[j][jj] = Matlab_result[j][jj] + 0.01
    X_ = []
    X = []
    for j in range(1,2200):
        X_.append(j/2000)
    for j in range(1,2000):
        X.append(j/2000)
        
    for i in range(1,L0.shape[0]): 
            f2 = interp1d(Matlab_mesh, Matlab_result[int(i), :] , kind='cubic')
            f = interp1d(X_, L0[i,:], kind='cubic')
            plot_function(X,abs(f(X)/f2(X) - 1),0,'r',
                          'Relative error between curves(%)',0,0,0,0,1)
            AREA.append(trapz(abs(f(X)/f2(X) - 1), dx=0.01))
    plt.show()
    for k in range(len(AREA)):
        AX.append(k)
    plot_function(AX,AREA,0,'Computing Step', 'Relative error(%)',0,1,1,0,0)
    A = sum(AREA)/len(AREA)
    print('Mean relative error is:')
    print(A)
    np.savetxt(FILESAVE, AREA, delimiter = ',')
