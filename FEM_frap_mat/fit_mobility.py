from celluloid import Camera
from dolfin import *
from fenics import *
from math import *
from mshr import *
from numpy import random
from scipy.interpolate import interp1d
from scipy.interpolate import RegularGridInterpolator as RGI
from scipy.optimize import minimize
from statistics import mean 

import matplotlib.animation as animation
import matplotlib.pyplot as plt
import numpy as np
import tifffile
import time

from fem_frap_mat import Form_expression,initial_condition,def_fenics_space

set_log_level(40)
parameters['allow_extrapolation'] = True

class InitCond(UserExpression):
            def __init__(self,kept_radius, background,Phi_max_sim,Phi_tot_mean,
                         r,Phi_tot_ext,interpolator):
                self.background = background
                self.Phi_max_sim = Phi_max_sim
                self.Phi_tot_mean = Phi_tot_mean
                self.r = r
                self.kept_radius = kept_radius
                self.Phi_tot_ext = Phi_tot_ext
                self.interpolator = interpolator
                super().__init__() 
            def eval(self, value, x):
                if sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]) <= self.kept_radius*self.r :
                    value[0] = (self.Phi_max_sim*(
                        self.interpolator([x[2], x[1], x[0]]) - self.background)/
                        (self.Phi_tot_mean - self.background))
                if (sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]) > self.kept_radius*self.r 
                 and sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]) <= self.r):
                            ### The translation (-0.35) need to be set manually
                    value[0] = (self.Phi_max_sim*(
                        self.interpolator([self.kept_radius*self.r*cos(theta(x[0], x[1],
                        x[2])),self.kept_radius*self.r*
                        sin(theta(x[0], x[1], x[2]))*sin(Phi(x[0],x[1])),
                        -0.35+ self.kept_radius*self.r*sin(theta(x[0], x[1], x[2])) *
                        cos(Phi(x[0],x[1]))]) - self.background)/
                        (self.Phi_tot_mean - self.background))
                if sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]) > self.r:
                    value[0] = self.Phi_tot_ext
            def value_shape(self):
                return ()
            
def save_coordinates(Point_evaluate,  Value, X_coordinate, X,
                     Y_coordinate, Y, Z_coordinate, Z):
    '''
    This function save the coordinates and the value of the 
    reference at these points in different .txt files.
    '''
    np.savetxt(Point_evaluate, Value[:,:], delimiter = ',')
    np.savetxt(X_coordinate, X, delimiter = ',')
    np.savetxt(Y_coordinate, Y, delimiter = ',')
    np.savetxt(Z_coordinate, Z, delimiter = ',')

def load_coordinates(X_coordinate, Y_coordinate, Z_coordinate, 
                     Point_evaluate):
    '''
    This function load the coordinates and the value of the
    reference at these points from different .txt files.
    '''
    X = np.genfromtxt(X_coordinate, delimiter=',')
    Y = np.genfromtxt(Y_coordinate, delimiter=',')
    Z = np.genfromtxt(Z_coordinate, delimiter=',')
    Value = np.genfromtxt(Point_evaluate, delimiter=',')

    return X, Y, Z, Value
    
def save_list_fit(TIME,List_e_g0,List_u_g0,List_root_mean_square,
                  Listpart,timee,e_g0, u_g0, Error, Partitioning,base_folder):
    '''
    This function save the List which are used to save the fitting 
    in different .txt files.
    '''
    TIME = np.append(TIME,[timee])
    List_e_g0= np.append(List_e_g0,[e_g0])
    List_u_g0 =np.append(List_u_g0,[u_g0])
    List_root_mean_square = np.append(List_root_mean_square,[mean(Error)])
    Listpart = np.append(Listpart,[Partitioning])
    np.savetxt(base_folder +'MeanRootMeansquare.txt', List_root_mean_square, 
               delimiter = ',')
    np.savetxt(base_folder +'eg0.txt', List_e_g0, delimiter = ',')
    np.savetxt(base_folder +'ug0.txt', List_u_g0, delimiter = ',')
    np.savetxt(base_folder+'Partitioning.txt', Listpart, delimiter = ',')
    np.savetxt(base_folder +'time.txt', TIME, delimiter = ',')
    
    
def load_list_fit(base_folder):
    '''
    This function load the List which are used to save the fitting 
    in different .txt files.
    '''
    try:
        List_root_mean_square = np.genfromtxt(base_folder +
                                        'MeanRootMeansquare.txt',delimiter=',')
        List_e_g0 = np.genfromtxt(base_folder +'eg0.txt', delimiter=',')
        List_u_g0 = np.genfromtxt(base_folder +'ug0.txt', delimiter=',')
        Listpart = np.genfromtxt(base_folder +'Partitioning.txt', delimiter=',')
        TIME = np.genfromtxt(base_folder +'time.txt', delimiter=',')
    except:
        List_root_mean_square =np.array([])
        List_e_g0 = np.array([])
        List_u_g0 = np.array([])
        Listpart = np.array([])
        TIME = np.array([])
        
    return List_root_mean_square, List_e_g0, List_u_g0, Listpart, TIME
    
def compute_sigma(Tif_file,Normalization_para,Mid_point_radius_file,
                  frame_before_FRAP):
    ''' 
     Allow to extract sigma(standard deviation ) from the data in order
     to simulate the statistical fluctuatioms
    '''
    tif, x, y, z, r = load_data_file(Tif_file,Mid_point_radius_file)
    sigma = initialize_sim_from_data(x, y, z,r, tif,Normalization_para,
                                     frame_before_FRAP,10,1)
    return sigma

def initialize_sim_from_data(x, y, z,r,tif,Normalization_para,
                             frame_before_FRAP,Partitioning,fluctuations):
    '''
     It calculate the mean of the frame before FRAP in order to normalize
     Compute Phi_tot_ext, Phi_tot_int
     It can extract from the real data the standard deviation in the droplet
    '''
    Phi_max_sim = Normalization_para[0]
    background = Normalization_para[1]
    Value_ = []
    X,Y,Z = points(0.8*r) #computing mean of Phi_tot_mean in the data
    interpolator_before_frap = RGI((z, y, x),
                                    tif[frame_before_FRAP], 
                                   bounds_error=False, fill_value=None)
    for j in range(len(X)):
        Value_.append(interpolator_before_frap([Z[j],Y[j], X[j]]))
    
    Phi_tot_mean = np.mean(Value_)        
    Phi_tot_int = Phi_max_sim*(Phi_tot_mean - background)/(
        Phi_tot_mean - background)
    
    for ll in range(len(Value_)):
        Value_[ll] = Phi_max_sim*(Value_[ll] - background)/(
            Phi_tot_mean - background)
    sigma = 0
    for kk in range(len(Value_)):
        sigma = sigma + (Value_[kk]-Phi_tot_int)**2
    sigma = sigma/len(Value_)
    sigma = sqrt(sigma)
    Phi_tot_ext =  Phi_tot_int/Partitioning
    if fluctuations == 1 :
        return sigma
    return Phi_tot_int, Phi_tot_ext,Phi_tot_mean

def theta(x,y,z):
    r = sqrt(x*x + y*y + z*z)
    return acos(z/r)
def Phi(x,y):
    return atan2(y,x)
    
def points(eval_radius):
    '''
    Create a grid in three dimensions and return a list with the coordinates of 
    the points below the radius eval_radius
    '''
    # use numpy from auto meshing
    nb_point = 20
    X_ = []
    X = []
    Y = []
    Z = []
    for i in range(int(nb_point/2)):
        X_.append(-2*eval_radius+i*eval_radius*4/nb_point)
    for k in range(int(nb_point/2)):
        X_.append(2*k*eval_radius*2/nb_point)
    for i in range(len(X_)):
        for j in range(len(X_)):
             for l in range(len(X_)):
                    if sqrt(X_[i]**2 +X_[j]**2+ X_[l]**2)<eval_radius:
                        X.append(X_[i])
                        Y.append(X_[j])
                        Z.append(X_[l])
    return X,Y,Z

def load_data_file(tif_file, mid_point_and_radius_file):
    '''
    Read the files and extract x,y,z,r and the tif array
    The function need to be change if we change the data
    '''
    tif = tifffile.imread(tif_file)
    x_mid, y_mid, z_mid, r = np.genfromtxt(mid_point_and_radius_file, 
                                           delimiter=',')
    x = 0.136 * np.arange(1, tif.shape[2] + 1, 1) - x_mid
    y = 0.136 * np.arange(1, tif.shape[3] + 1, 1) - y_mid
    z = 0.5 * np.arange(1, tif.shape[1] + 1, 1) - z_mid
    return tif, x, y, z, r

def plot_function(X,Y,legend,x_label, y_label,log):
    '''
    Function used for plot different function
    '''
    plt.plot(X,Y, label = legend)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    if log == 1 :
        plt.yscale('log')
    plt.legend()
    plt.plot()
    plt.show()

def plot_Phi_u(Phi_u0, vector,R_plot, data_or_sim, interpolator,
               Normalization_para,Phi_tot_mean):
    '''
    Plot Phi_u along the vector choose, bewteen -R_plot and R_plot
    '''
    Phi_max_sim = Normalization_para[0]
    background = Normalization_para[1]
    tol = 0.0001
    x1= np.linspace(-R_plot + tol, R_plot - tol, 2000)
    if data_or_sim == 1:
        Phi_u0_ = np.array([(Phi_max_sim*(interpolator([point]) - background)/(
                            Phi_tot_mean - background)) for point in vector])
        plot_function( x1, Phi_u0_,'Real Data','r', r'$\phi_{u}$',0)
    if data_or_sim == 2:
        Phi_u0_ = np.array([Phi_u0(point) for point in vector])
        plot_function( x1, Phi_u0_,'FEM','r', r'$\phi_{u}$',0)

def vector(a,b,c,R_):
    '''
    Create a vector of length 2*R_ ,between -R_ and R_ 
    along the axis a*x, b*y, c*z
    '''
    ## a,b,c determine the direction of the plot
    ## R_plot determine the radius of plot
    tol = 0.0001
    x1= np.linspace(-R_ + tol, R_- tol, 2000)
    vector_ = [(a*x_, b*x_, c*x_) for x_ in x1]
    return vector_, R_

def stock_results(Phi_u0, save_vector_,R_save, data_or_sim, 
                  interpolator,Normalization_para,Phi_tot_mean):
    '''
    Allow to create an array in order to save the result
    '''
    Phi_max_sim, background = Normalization_para[0],Normalization_para[1]
    L = []
    tol = 0.0001
    x1= np.linspace(-R_save + tol, R_save - tol, 2000)
    if data_or_sim == 2:
        Phi_u0_ = np.array([Phi_u0(point) for point in save_vector_]) 
    if data_or_sim == 1:
        Phi_u0_ = np.array([interpolator(point) for point in save_vector_])   
        Phi_u0_ = (Phi_max_sim*(Phi_u0_ - background)/(Phi_tot_mean - background))
    f = interp1d(x1,Phi_u0_,kind='cubic')
    for j in range(1, int(1000*R_save)):
        L.append(f((-1000*(R_save - tol)+j)/1000))
    for j in range(1,int(1000*R_save)):
        L.append(f(j/1000 - tol ))
    return L

def plot_save_result(R_save, *arg):
    '''
    Plot the result from the save file containing the python results along a line 
    The parameters R_save need to be in adequation with the results 
    '''
    L,X =[],[]
    tol = 0.0001
    for n in arg:
        temp = np.genfromtxt(n, delimiter=',')
        L.append(temp)
    for j in range(1, int(1000*R_save)):
        X.append(((-1000*(R_save- tol )+j)/1000))
    for j in range(1,int(1000*R_save)):
        X.append((j/1000  - tol))
    L0 = L[0]
    for i in range(L0.shape[0]):
        for j in range(len(L)):
            LISTE = L[j]
            plt.plot(X, LISTE[i,:],  label = 'Simulation %d' %(j+1))
        plt.ylabel(r'$\phi_{u}$')
        plt.xlabel('r')  
        plt.legend()
        plt.show()

def video(R_save, video_save, *arg):
    '''
    Plot the result from the save file containing the python results along 
    a line and make a video from it 
    The parameters R_save need to be in adequation with the results 
    '''
    fig = plt.figure()
    camera = Camera(fig)
    cycle = plt.rcParams['axes.prop_cycle'].by_key()['color']
    L, X = [], []
    tol = 0.0001
    for n in arg:
        temp = np.genfromtxt(n, delimiter=',')
        L.append(temp)
    for j in range(1, int(1000*R_save)):
        X.append(((-1000*(R_save- tol )+j)/1000))
    for j in range(1,int(1000*R_save)):
        X.append((j/1000  - tol))
    L0 = L[0]
    for i in range(L0.shape[0]):
        for j in range(len(L)):
            LISTE = L[j]
            plt.plot(X, LISTE[i,:], color=cycle[j], label = '%d' %(j+1))

            if i == 0:
                    plt.legend(bbox_to_anchor=(0.5, 0.9), 
                               loc='upper left', borderaxespad=0.)
            else:
                pass
        plt.ylabel(r'$\phi_{u}$')
        plt.xlabel('r')  
            
        camera.snap()
    animation = camera.animate()
    animation.save(video_save)

    
def save_parameters(txt_file, frame_ic, frame_final,Tif_file, type_data, noise,
                    parameters,Normalization_para, mesh_file):
    '''
    This function allow to save the parameters used in a txt file
    '''
    text_file = open(txt_file, 'w')

    if frame_final != 0:
        n = text_file.write('Points are created from the experimental data\n')
        n = text_file.write('The tif file used is: ')
        n = text_file.write(str(Tif_file))
        n = text_file.write('\nThe first frame used is: ')
        n = text_file.write(str(frame_ic))
        n = text_file.write('\nThe last frame used is: ')
        n = text_file.write(str(frame_final))
    
    if type_data == 0:
        n = text_file.write('Points are created from the experimental data '+
                            'as initial conditions\n')
        n = text_file.write('The tif file used is: ')
        n = text_file.write(str(Tif_file))
        n = text_file.write('\nThe first frame used is: ')
        n = text_file.write(str(frame_ic))
        
    if type_data == 1:
        n = text_file.write('\nPoints are created with full frap as '+
                            'initial conditions')
    
    if type_data == 2:
        n = text_file.write('\nPoints are created with half frap as '+
                            'initial conditions')

        
    n = text_file.write('\nThe parameters used in the simulation are :\n')
    n = text_file.write(str(parameters))
    n = text_file.write('\nThe normalization parameters used in the '+
                        'simulation are :\n')
    n = text_file.write(str(Normalization_para))

    if noise != 0:
        n = text_file.write('\nA noise is added, of standard deviation: ')
        n = text_file.write(str(parameters[9]))
    else: 
        n = text_file.write('\nNo noise has been add ')

    n = text_file.write('\nThe mesh used is: ')
    n = text_file.write(str(mesh_file))
    
    text_file.close()
    
    
def data_to_mesh(Plot, Save,Save_Parameters, save_parameters_file,
                 Normalization_para, frame_ic,frame_final, 
                 frame_before_FRAP, Tif_file, Mid_point_radius_file,
                 save_file,eval_radius,plot_vector_,R_plot,save_vector_,R_save,
                 Point_evaluate, X_coordinate, Y_coordinate, Z_coordinate ):
    '''
    Create the point from the data that will be use as reference and save them 
    We can save/plot the data along one axis
    It saves the coordinates of the points inside the files X_coordinate,
    Y_coordinate, Z_coordinate (.txt)
    It saves the value of the points inside the file Point_evaluate (.txt)
    Plot: 0 (no plot)or 1(activate ploting) along one axis
    Save: 0 (no save)or 1(activate save) along one axis
    Save_Parameters:  0 (no save)or 1(activate save) of the parameters used
    save_parameters_file: file (.txt) where the parameters used are saved
    Normalization_para : [Phi_max_sim, background] : maximum value used in
    the simulation and offset induce by measurement
    frame_ic: frame from the data use as initial conditions
    frame_final: final frame from the data that will be used
    frame_before_FRAP: Put a frame before FRAP in order 
    to initialize the simulation
    Tif_file: tif file which contain the data
    Mid_point_radius_file: file which contain the mid points and 
    the radius of the droplet
    save_file: file (.txt) where the data along one axis is saved
    eval_radius: coefficient lower than 1,
    the point evalued will be under this value*radius of the droplet
    plot_vector_: Vector along which the plot will be made
    R_plot: the lenght of the previous vector will be 2*R_plot 
    save_vector_: Vector along which the save will be made
    R_save: the lenght of the previous vector will be 2*R_plot 
    '''
    t = 0
    List_Result,Value,Valuebis,Value_ = [], [], [], []
    tif, x, y, z, r = load_data_file(Tif_file, Mid_point_radius_file)
    interpolator_ic = RGI((z, y, x), tif[frame_ic], 
                                bounds_error=False, fill_value=None)
    Phi_max_sim, background = Normalization_para[0],Normalization_para[1]
    
    X,Y,Z = points(r*0.8) #computing mean of Phi_tot 
    interpolator_before_frap = RGI((z, y, x), tif[frame_before_FRAP], 
                                    bounds_error=False, fill_value=None)
    for j in range(len(X)):
        Value_.append(interpolator_before_frap([Z[j],Y[j], X[j]]))
    Phi_tot_mean = np.mean(Value_)
    X,Y,Z = points(r*eval_radius) 
    for i in range (frame_ic,frame_final+1):
        interpolator = RGI((z, y, x), tif[i],
                                        bounds_error=False, fill_value=None)
        if i == frame_ic:
            for j in range(len(X)):
                Value.append((Phi_max_sim*(
                    interpolator([Z[j],Y[j], X[j]]) - background)/(
                                Phi_tot_mean - background))[0])
        else:
            for j in range(len(X)):
                Valuebis.append((Phi_max_sim*(
                    interpolator([Z[j],Y[j], X[j]]) - background)/(
                                Phi_tot_mean - background))[0])
            Value = np.vstack([Value, Valuebis])
            Valuebis = []
        if Plot == 1:
            plot_Phi_u(0, plot_vector_,R_plot, 1, interpolator,
                       Normalization_para,Phi_tot_mean)
        if Save ==1:
            if i == frame_ic:
                List_Result = stock_results(0, save_vector_,R_save, 1, 
                                interpolator,Normalization_para,Phi_tot_mean)
            else:
                List_Result = np.vstack((List_Result, stock_results(0, save_vector_,
                        R_save, 1, interpolator,Normalization_para,Phi_tot_mean)))
    if Save_Parameters == 1:
        save_parameters(save_parameters_file, frame_ic, frame_final,
                        Tif_file, 5, 0, 'none',Normalization_para, 'none')
    if Save == 1:
        np.savetxt(save_file, List_Result[:,:], delimiter = ',')
    save_coordinates(Point_evaluate,  Value, X_coordinate, X,
                     Y_coordinate, Y, Z_coordinate, Z)
    
def add_noise_inside(Phi_u0, V, sigma,a):
    '''
    This function allow to add gaussian white noise of standard deviation 
    sigma inside the droplet
    '''
    w_array = Phi_u0.vector()
    dof_coordinates = V.tabulate_dof_coordinates()
    dof_x = dof_coordinates[:, 0]                                                    
    dof_y = dof_coordinates[:, 1]  
    dof_z = dof_coordinates[:, 2]  
    for ll in range(len(dof_x)):
        if sqrt(dof_x[ll]**2 + dof_y[ll]**2 + dof_z[ll]**2) < -a :
            noise = np.random.normal(0,sigma,1)
            w_array[ll] = w_array[ll] + noise[0]
            if w_array[ll] < 0:
                w_array[ll] = abs(w_array[ll])
    return Phi_u0
    
def Artificial_data_set(type_data, noise, Plot, Save, Save_Paraview,
                        Save_Parameters, save_parameters_file,parameters,
                        Normalization_para,Tif_file,Mid_point_radius_file,
                        frame_before_FRAP,frame_ic,eval_radius,kept_radius,
                        mesh_file, save_file,xdmf_file, plot_vector_, R_plot,
                        save_vector_, R_save,Point_evaluate, X_coordinate,
                        Y_coordinate, Z_coordinate): 
    '''
    Create an artificial data set with free parameters
    type_data=0: initial condition extract from real data
    type_data=1: initial condition full FRAP
    type_data=2: initial condition half FRAP
    noise =1 : Add statistical fluctuations inside the droplet
    It saves the coordinates of the points inside the files
    X_coordinate, Y_coordinate, Z_coordinate (.txt)
    It saves the value of the points inside the file Point_evaluate (.txt) 
    Plot: 0 (no plot)or 1(activate ploting) along one axis
    Save: 0 (no save)or 1(activate save) along one axis
    Save_Paraview:  0 (no save)or 1(activate save) on Paraview (.xdmf)
    Save_Parameters:  0 (no save)or 1(activate save) of the parameters used
    parameters: parameters used in the simulation
    Normalization_para: parameters used in the normalization
    Tif_file: tif file which contain the data
    Mid_point_radius_file: file which contain the mid points and the radius 
    of the droplet
    frame_before_FRAP: frame before FRAP in order to initialize the simulation
    frame_ic: frame from the data use as initial conditions
    eval_radius: coefficient lower than 1,
    the point evalued will be under this value*radius of the droplet
    kept_radius: coefficient lower than 1, the extrapolation, 
    to avoid optical aberrations,is done beyond this value*radius of the droplet
    mesh_file: mesh used (.xml)
    save_file: file (.txt) where the data along one axis is saved
    xdmf_file: file(.xdmf) where the simulation is saved
    save_parameters_file: file (.txt) where the parameters used are saved
    plot_vector_: Vector along which the plot will be made
    R_plot: the lenght of the previous vector will be 2*R_plot 
    save_vector_: Vector along which the save will be made
    R_save: the lenght of the previous vector will be 2*R_plot 
    '''
    V,v, Phi_u =  def_fenics_space(mesh_file)
    (a,b,e,u0,e_g0,u_g0,tf,dt, Partitioning, sigma) = (parameters[0],
                                    parameters[1],parameters[2], parameters[3],
                                    parameters[4],parameters[5], parameters[6],
                                    parameters[7],parameters[8], parameters[9])
    Nb_step = int(tf/dt)
    Value,Valuebis,List_Result = [],[],[]
    X,Y,Z = points(-a*eval_radius) # the points are inside the droplet
    t = 0
    Phi_max_sim,background= Normalization_para[0],Normalization_para[1]
    Phi_tot_mean = 0
    
    if type_data == 0:
        tif, x, y, z, r = load_data_file(Tif_file,Mid_point_radius_file)
        Phi_tot_int, Phi_tot_ext,Phi_tot_mean = initialize_sim_from_data(x, y,
                                    z, r,tif,Normalization_para,frame_before_FRAP,
                                    Partitioning,0) 
        Phi_u0 = Function(V)
        interpolator = RGI((z, y, x), tif[frame_ic], bounds_error=False,
                           fill_value=None)
        u0 = (Phi_tot_int + Phi_tot_ext)/2
        e = (Phi_tot_int - Phi_tot_ext)/2       
        f_init = InitCond(kept_radius, background , Phi_max_sim, Phi_tot_mean,
                          r, Phi_tot_ext,interpolator)
        Phi_u0 = interpolate(f_init,V)
        para = [a, b, e, u0, e_g0, u_g0, tf ,dt,Partitioning,sigma]
        Phi_tot,useless,Gamma_0 = initial_condition(3, para, V, 0) 
        
    if type_data == 1:
        Phi_tot,Phi_u0,Gamma_0 = initial_condition(3, parameters, V, 0)
        
    if type_data == 2:
        Phi_tot,Phi_u0,Gamma_0 = initial_condition(3, parameters, V, 1)
        
    Form = Form_expression(Phi_u,Phi_u0,Gamma_0,Phi_tot,dt,v)
    
    if Save_Paraview == 1:
        cFile = XDMFFile(xdmf_file)
        
    for i in range(0,Nb_step +1):   
        if noise == 1:
            if i != 0:
                Phi_u0 = add_noise_inside(Phi_u0, V, sigma,a)                            
        if Plot ==1 :
            plot_Phi_u(Phi_u0, plot_vector_,R_plot, 2, 0,Normalization_para,
                       Phi_tot_mean)
        if Save ==1:
            if i ==0:
                List_Result = stock_results(Phi_u0, save_vector_,R_save, 2, 0,
                                            Normalization_para,Phi_tot_mean)
            else:
                List_Result = np.vstack((List_Result, stock_results(Phi_u0,
                                        save_vector_,R_save, 2,0,
                                        Normalization_para,Phi_tot_mean)))   
        if Save_Paraview == 1:
            cFile.write(Phi_u0, t)
        if i == 0:
            for j in range(len(X)):
                Value.append(Phi_u0(X[j],Y[j],Z[j]))
        else:
            for j in range(len(X)):
                Valuebis.append(Phi_u0(X[j],Y[j],Z[j]))
            Value = np.vstack([Value, Valuebis])
            Valuebis = []
        solve(Form == 0, Phi_u)#, solver_parameters={'newton_solver': {'linear_solver': 'gmres'}})
        assign(Phi_u0, Phi_u)
        t += dt
    if Save_Paraview == 1:
        cFile.close()
    if Save_Parameters == 1:
        save_parameters(save_parameters_file, frame_ic,0, Tif_file, type_data, 
                        noise, parameters,Normalization_para, mesh_file)
    if Save == 1:
        np.savetxt(save_file, List_Result[:,:], delimiter = ',') 
    save_coordinates(Point_evaluate,  Value, X_coordinate, X, Y_coordinate,
                     Y, Z_coordinate, Z)

def change_phi_outside(x0,Phi_tot_int,Phi_u0,space, a):
    '''
    This function change the value outside the droplet
    in adequation with the Partitioning
    '''
    Partitioning = x0[2]
    Phi_tot_ext = Phi_tot_int/Partitioning
    u0 = (Phi_tot_int + Phi_tot_ext)/2
    e = (Phi_tot_int - Phi_tot_ext)/2
    w_array = Phi_u0.vector()

    dof_coordinates = space.tabulate_dof_coordinates()
    dof_x = dof_coordinates[:, 0]                                                    
    dof_y = dof_coordinates[:, 1]  
    dof_z = dof_coordinates[:, 2]  

    for i in range(len(dof_x)):
        if sqrt(dof_x[i]**2 + dof_y[i]**2 + dof_z[i]**2) > -a :
            w_array[i] = Phi_tot_ext
            
    return Phi_u0
    
def error_real_data(x0,_3_para, parameters,Phi_tot_int, Phi_tot_ext,Phi_u0_, 
                            Phi_u_,v,space,X_coordinate,Y_coordinate,
                            Z_coordinate,Point_evaluate, base_folder ):
    '''
    Launch a simulation with mobility parameters in x0
    Compute the error function between reference points and this simulation
    The inital conditions need to be done before with the IC_once function
    It is use inside the fitting function
    It saves the result of the fitting inside a folder
    that we need to specify (base_folder)
    It uses as reference the coordinates of the points inside the files 
    X_coordinate, Y_coordinate, Z_coordinate (.txt)
    It uses as reference the value of the points inside the file Point_evaluate 
    
    x0: eg0, ug0, Partition -> allow to compute mobility
    _3_para =1: use 3 para for the fit
    parameters: parameters used in the simulation, the mobility are not taken
    Phi_tot_int: extract from IC_once
    Phi_tot_ext: extract from IC_once
    Phi_u0_: extract from IC_once
    Phi_u_: extract from IC_once
    v: extract from IC_once
    space: extract from IC_once
    '''
    List_root_mean_square, List_e_g0, List_u_g0, Listpart,TIME = load_list_fit(
        base_folder)
    t = 0
    ti = time.time()
    Error,List_Result = [], []

    (a, b, tf, dt, Partitioning) = (parameters[0], parameters[1], parameters[6], 
                                 parameters[7], parameters[8])
    
    e_g0,u_g0 = x0[0],x0[1]
    u0 = (Phi_tot_int + Phi_tot_ext)/2
    e = (Phi_tot_int - Phi_tot_ext)/2
    Phi_u0 = Function(space)
    Phi_u = Function(space)
    assign(Phi_u0,Phi_u0_)
    assign(Phi_u,Phi_u_)
    ti = time.time()
    t, temp = 0, 0
    Nb_step = int(tf/dt)
    if u_g0 <0:
        M = 10000000
        return M
    if _3_para == 1:
        Partitioning = x0[2]
        if Partitioning <1:
            M = 10000000
            return M
        Phi_u0 = change_phi_outside(x0,Phi_tot_int,Phi_u0,space, a)
        
    X,Y,Z,Value = load_coordinates(X_coordinate, Y_coordinate, 
                                   Z_coordinate, Point_evaluate)
        
    para = [a, b, e, u0, e_g0, u_g0, tf ,dt]
    Phi_tot,useless,Gamma_0 = initial_condition(3, para, space, 0) 
    
    Phi_tot = change_phi_outside(x0,Phi_tot_int,Phi_tot,space, a)    
    
    Form = Form_expression(Phi_u,Phi_u0,Gamma_0,Phi_tot, dt,v)
 
    for i in range(0,Nb_step +1):
        for ii in range(len(X)):
            temp = temp + (Value[int(i)][ii]- Phi_u0(X[ii],Y[ii],Z[ii]))**2
        temp = sqrt(temp/len(X))
        Error.append(temp)
        temp = 0
        solve(Form == 0, Phi_u)#, solver_parameters={'newton_solver': {'linear_solver': 'gmres'}})
        assign(Phi_u0, Phi_u)
        t += dt
    print('e_g0 and ug_0 are :')
    print(e_g0)
    print('and')
    print(u_g0)
    if _3_para == 1:
        print(' P is: ')
        print(Partitioning)
    timee = time.time() - ti
    save_list_fit(TIME,List_e_g0,List_u_g0,List_root_mean_square,Listpart,
                  timee,e_g0, u_g0, Error, Partitioning,base_folder)
    M = mean(Error)
    print('M is :')
    print(M)
    return M
    
def error_artificial_data(x0,type_data, parameters, mesh_file, X_coordinate,
                    Y_coordinate, Z_coordinate,Point_evaluate, base_folder ):
    '''
    Launch a simulation with mobility parameters in x0
    Compute the error function between reference points
    (here those extract from Artificial_data_set) and this simulation
    It is use inside the fitting function
    It saves the result of the fitting inside a folder that 
    we need to specify (base_folder)
    It uses as reference the coordinates of the points inside the files 
    X_coordinate, Y_coordinate, Z_coordinate (.txt)
    It uses as reference the value of the points inside the file Point_evaluate
    
    x0: eg0, ug0, Partition -> allow to compute mobility
    type_data=1: initial condition full FRAP
    type_data=2: initial condition half FRAP
    parameters: parameters used in the simulation, the mobility are not taken
    mesh_file: mesh use for the simulation
    '''
    List_root_mean_square, List_e_g0, List_u_g0, Listpart, TIME = load_list_fit(
        base_folder)
    t = 0
    ti = time.time()
    
    V,v, Phi_u =  def_fenics_space(mesh_file)

    (a,b,e,u0,tf,dt,Partitioning) = (parameters[0], parameters[1],parameters[2],
                    parameters[3], parameters[6],parameters[7],parameters[8])

    e_g0, u_g0= x0[0], x0[1]
    
    if u_g0 <0:
        M = 10000000
        return M
    Error,List_Result = [], []
    ti = time.time()
    t, temp = 0, 0
    Nb_step = int(tf/dt)

    X,Y,Z,Value = load_coordinates(X_coordinate, Y_coordinate, Z_coordinate,
                                   Point_evaluate)  
            
    para = [a, b, e, u0, e_g0, u_g0, tf ,dt,Partitioning]
    
    if type_data == 1: 

        Phi_tot,Phi_u0,Gamma_0 = initial_condition(3, para, V, 0)

    if type_data == 2:
        Phi_tot,Phi_u0,Gamma_0 = initial_condition(3, para, V, 1)
        
    Form = Form_expression(Phi_u,Phi_u0,Gamma_0,Phi_tot, dt,v)
 
    for i in range(0,Nb_step +1):     
        for ii in range(len(X)):
            temp = temp + (Value[int(i)][ii]- Phi_u0(X[ii],Y[ii],Z[ii]))**2
        temp = sqrt(temp/len(X))
        Error.append(temp)
        temp = 0
        
        solve(Form == 0, Phi_u)#, solver_parameters={'newton_solver': {'linear_solver': 'gmres'}})
        assign(Phi_u0, Phi_u)
        t += dt
        
    timee = time.time() - ti
    save_list_fit(TIME,List_e_g0,List_u_g0,List_root_mean_square,Listpart,timee,
                  e_g0, u_g0, Error, Partitioning,base_folder)
    M = mean(Error)
    print('M is :')
    print(M)
    return M

def IC_once(parameters,Normalization_para, File_mesh,Tif_file,
            Mid_point_radius_file,frame_before_FRAP,frame_ic, kept_radius):
    '''
    This function is an initialization function which allow to compute 
    only one time the interpolation between the real data and the simulation,
    even if we simulate repeatedly
    It returns the initial conditions (Phi_u01) and the function 
    that we will solve 
    (Phi_u1) in the resolution space (V)
    It returns the test function (v) 
    It returns the value of Phi_tot_in and Phi_tot_ext extract from 
    the experimental data

    File_mesh: mesh used for the computing (.xml)
    Tif_file: tif file which contain the data
    Mid_point_radius_file: file which contain the mid points and 
    the radius of the droplet
    frame_before_FRAP: Put a frame before FRAP in order to initialize
    the simulation
    frame_ic: frame from the data use as initial conditions
    kept_radius: coefficient lower than 1, the extrapolation, 
    to avoid optical aberrations,
    is done beyond this value*radius of the droplet
    '''
    Partitioning = parameters[8]
    tif, x, y, z, r = load_data_file(Tif_file,Mid_point_radius_file)
    Phi_tot_int, Phi_tot_ext,Phi_tot_mean = initialize_sim_from_data(x, y, z,
                                            r,tif,Normalization_para,frame_before_FRAP,
                                            Partitioning,0) 
    V,v, Phi_u1 =  def_fenics_space(File_mesh)
    Phi_u01 = Function(V)
    Phi_max_sim = Normalization_para[0]
    background = Normalization_para[1]
    interpolator = RGI((z, y, x), tif[frame_ic], bounds_error=False,
                       fill_value=None)
    f_init = InitCond(kept_radius, background , Phi_max_sim, Phi_tot_mean,
                      r, Phi_tot_ext,interpolator)
    Phi_u01 = interpolate(f_init,V)
    return Phi_u01, Phi_u1, V, v,  Phi_tot_int, Phi_tot_ext


def save_fit_results(res,base_folder,seed_parameters):
    '''
    This function save the results of the fitting inside a text file
    '''
    text_file = open(base_folder + 'res.txt', 'w')
    n = text_file.write('Initial condition are : ')
    n = text_file.write(str(seed_parameters))
    n = text_file.write('\nresults are x0 (e_g0 and ug0): ')
    n = text_file.write(str(res.x))
    n = text_file.write('\nValue of the function is : ')
    n = text_file.write(str(res.fun))
    n = text_file.write('\nnumber of function called is: ')
    n = text_file.write(str(res.nfev))
    n = text_file.write('\nnumber of gradient called is: ')
    n = text_file.write(str(res.nit))
    n = text_file.write('\nmessage :')
    n = text_file.write(res.message)
    text_file.close()
    
def fit_artificial_frap(type_data,seed_parameters,parameters,File_mesh,
                             X_coordinate,Y_coordinate,Z_coordinate,
                                    Point_evaluate, base_folder):
    '''
    This function allow to fit a set of artificial data
    type_data=1: initial condition full FRAP
    type_data=2: initial condition half FRAP
    seed_parameters = parameters use a seed for the fitting
    parameters: parameters used in the simulation
    File_mesh: mesh used (.xml)
    It used the coordinates of the points inside the files
    X_coordinate, Y_coordinate, Z_coordinate (.txt) as ref
    It used the value of the points inside the file Point_evaluate (.txt) 
    It save all kind of result in the base folder choose 
    '''
    x0 = [seed_parameters[0], seed_parameters[1]]
    if type_data == 1 or type_data == 2:
        res = minimize(error_artificial_data,x0, 
                       args=( type_data,parameters,File_mesh,X_coordinate,
                             Y_coordinate,Z_coordinate,Point_evaluate,
                             base_folder),method = 'Nelder-Mead',tol = 0.00001,
                             options={'disp': True,'maxiter':100}) 
    print('optimize x0 is :')
    print(res)
    print(res.x)
    save_fit_results(res,base_folder,seed_parameters)
    
def fit_data_from_exp(_3_para,seed_parameters,parameters,frame_ic, kept_radius,
                      Phi_tot_int, Phi_tot_ext,Phi_u0_, Phi_u_,v,space,
                      X_coordinate,Y_coordinate,Z_coordinate,Point_evaluate,
                      base_folder):
    '''
    This function allow to fit a set of data with real initial conditions
    _3_para =1: use 3 para for the fit
    seed_parameters = parameters use a seed for the fitting
    parameters: parameters used in the simulation
    File_mesh: mesh used (.xml)
    It used the coordinates of the points inside the files
    X_coordinate, Y_coordinate, Z_coordinate (.txt) as ref
    It used the value of the points inside the file Point_evaluate (.txt) 
    It save all kind of result in the base folder choose 
    '''
    if _3_para == 1:
        x0 = [seed_parameters[0], seed_parameters[1], seed_parameters[2] ]
        res = minimize(error_real_data,x0, args=(_3_para,parameters,
                                Phi_tot_int,Phi_tot_ext,Phi_u0_,
                                Phi_u_,v,space,X_coordinate,Y_coordinate,
                                Z_coordinate,Point_evaluate, base_folder),
                               method = 'Nelder-Mead',tol = 0.00001,options={
                               'disp': True,'maxiter':300}) 
        
    else:
        x0 = [seed_parameters[0], seed_parameters[1]]
        res = minimize(error_real_data,x0, args=(
            _3_para,parameters,Phi_tot_int,Phi_tot_ext,Phi_u0_,Phi_u_,
            v,space,X_coordinate,Y_coordinate,Z_coordinate,Point_evaluate,
            base_folder),method = 'Nelder-Mead',tol = 0.00001,options={
            'disp': True,'maxiter':100}) 
    print('optimize x0 is :')
    print(res)
    print(res.x)
    save_fit_results(res,base_folder,seed_parameters)

def plot_conv(base_folder):
    '''
    Plot the convergence of a fitting for all the parameters 
    and for the error function
    '''
    f = open(base_folder + 'res.txt', "r")
    print(f.read())
    
    TIME = []
    Mean_square_it, eg0, ug0, Partitioning, time = load_list_fit(base_folder)
    
    s= 0
    for i in range(len(time)):
        s = s + time[i]

        TIME.append(s)
      
    plot_function(TIME,Mean_square_it,'Convergence','Computing time',
                  'Mean RMS',1)
    plot_function(TIME,eg0,'Convergence','Computing time','eg0',0)
    plot_function(TIME,ug0,'Convergence','Computing time','ug0',0)
    plot_function(TIME,Partitioning,'Convergence','Computing time',
                  'Partitioning',0)
    plot_function(ug0,eg0+ug0,'Convergence in space parameters','mobility in', 
                  'mobility out',0)
    