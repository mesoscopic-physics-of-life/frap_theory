# trace generated using paraview version 5.7.0
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'Xdmf3ReaderS'
multi_drop_6_neighbors_farxdmf = Xdmf3ReaderS(FileName=['/Users/hubatsch/Desktop/frap_theory/Visualization/multi_drop_6_neighbors_far.xdmf'])
multi_drop_6_neighbors_farxdmf.PointArrays = ['f_5785']

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [926, 531]

# show data in view
multi_drop_6_neighbors_farxdmfDisplay = Show(multi_drop_6_neighbors_farxdmf, renderView1)

# get color transfer function/color map for 'f_5785'
f_5785LUT = GetColorTransferFunction('f_5785')

# get opacity transfer function/opacity map for 'f_5785'
f_5785PWF = GetOpacityTransferFunction('f_5785')

# trace defaults for the display properties.
multi_drop_6_neighbors_farxdmfDisplay.Representation = 'Surface'
multi_drop_6_neighbors_farxdmfDisplay.ColorArrayName = ['POINTS', 'f_5785']
multi_drop_6_neighbors_farxdmfDisplay.LookupTable = f_5785LUT
multi_drop_6_neighbors_farxdmfDisplay.OSPRayScaleArray = 'f_5785'
multi_drop_6_neighbors_farxdmfDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
multi_drop_6_neighbors_farxdmfDisplay.SelectOrientationVectors = 'None'
multi_drop_6_neighbors_farxdmfDisplay.ScaleFactor = 0.8
multi_drop_6_neighbors_farxdmfDisplay.SelectScaleArray = 'f_5785'
multi_drop_6_neighbors_farxdmfDisplay.GlyphType = 'Arrow'
multi_drop_6_neighbors_farxdmfDisplay.GlyphTableIndexArray = 'f_5785'
multi_drop_6_neighbors_farxdmfDisplay.GaussianRadius = 0.04
multi_drop_6_neighbors_farxdmfDisplay.SetScaleArray = ['POINTS', 'f_5785']
multi_drop_6_neighbors_farxdmfDisplay.ScaleTransferFunction = 'PiecewiseFunction'
multi_drop_6_neighbors_farxdmfDisplay.OpacityArray = ['POINTS', 'f_5785']
multi_drop_6_neighbors_farxdmfDisplay.OpacityTransferFunction = 'PiecewiseFunction'
multi_drop_6_neighbors_farxdmfDisplay.DataAxesGrid = 'GridAxesRepresentation'
multi_drop_6_neighbors_farxdmfDisplay.PolarAxes = 'PolarAxesRepresentation'
multi_drop_6_neighbors_farxdmfDisplay.ScalarOpacityFunction = f_5785PWF
multi_drop_6_neighbors_farxdmfDisplay.ScalarOpacityUnitDistance = 0.16444562331461687

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
multi_drop_6_neighbors_farxdmfDisplay.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.9900000095367432, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
multi_drop_6_neighbors_farxdmfDisplay.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.9900000095367432, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# show color bar/color legend
multi_drop_6_neighbors_farxdmfDisplay.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

#change interaction mode for render view
renderView1.InteractionMode = '2D'

#change interaction mode for render view
renderView1.InteractionMode = '3D'

# create a new 'Slice'
slice1 = Slice(Input=multi_drop_6_neighbors_farxdmf)
slice1.SliceType = 'Plane'
slice1.SliceOffsetValues = [0.0]

# init the 'Plane' selected for 'SliceType'
slice1.SliceType.Origin = [4.0, 4.0, 4.0]

# Properties modified on slice1.SliceType
slice1.SliceType.Origin = [4.0, 4.0, 0.5]
slice1.SliceType.Normal = [0.0, 0.0, 1.0]

# show data in view
slice1Display = Show(slice1, renderView1)

# trace defaults for the display properties.
slice1Display.Representation = 'Surface'
slice1Display.ColorArrayName = ['POINTS', 'f_5785']
slice1Display.LookupTable = f_5785LUT
slice1Display.OSPRayScaleArray = 'f_5785'
slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
slice1Display.SelectOrientationVectors = 'None'
slice1Display.ScaleFactor = 0.8
slice1Display.SelectScaleArray = 'f_5785'
slice1Display.GlyphType = 'Arrow'
slice1Display.GlyphTableIndexArray = 'f_5785'
slice1Display.GaussianRadius = 0.04
slice1Display.SetScaleArray = ['POINTS', 'f_5785']
slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
slice1Display.OpacityArray = ['POINTS', 'f_5785']
slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
slice1Display.DataAxesGrid = 'GridAxesRepresentation'
slice1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
slice1Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.9900000095367432, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
slice1Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 0.9900000095367432, 1.0, 0.5, 0.0]

# hide data in view
Hide(multi_drop_6_neighbors_farxdmf, renderView1)

# show color bar/color legend
slice1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [7.7195210119804365, 6.020340271519015, 37.20173938769656]
renderView1.CameraFocalPoint = [11.12731275349468, 8.382283791317974, 5.07831287324817]
renderView1.CameraViewUp = [0.010264079591080513, 0.9971750379476707, 0.07440828155662131]
renderView1.CameraParallelScale = 8.383125908633367

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).