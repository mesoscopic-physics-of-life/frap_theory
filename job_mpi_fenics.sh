#!/bin/bash
#
# Number of nodes
#SBATCH -N 1
#SBATCH --ntasks=1
#
# Use nodes exclusive
##SBATCH --exclusive
#
#SBATCH --time=00:10:00
#
#SBATCH --cpus-per-task=5
#SBATCH --mem-per-cpu=10G
#SBATCH --error %j.out


# Load the module environment suitable for the job
source activate fepy37

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
start=`date +%s`
srun --mpi=pmi2 python /home/hubatsch/fenics_mpi/frap_theory/fenics_mpi_example.py
end=`date +%s`
runtime=$((end-start))
echo $runtime
