//+
lc = 1;
//+
Point(4) = {0, 5, 0, lc};
//+
Point(3) = {5, 5, 0, lc};
//+
Point(2) = {5, 0, 0, lc};
//+
Point(1) = {0, 0, 0, lc};
//+
Point(8) = {0, 5, 10, lc};
//+
Point(7) = {5, 5, 10, lc};
//+
Point(6) = {5, 0, 10, lc};
//+
Point(5) = {0, 0, 10, lc};
//+
Point(9) = {0.5, 0.5, 0, lc};
//+
Line(6) = {2, 1};
//+
Line(5) = {3, 2};
//+
Line(8) = {4, 3};
//+
Line(7) = {1, 4};
//+
Line(3) = {6, 5};
//+
Line(2) = {7, 6};
//+
Line(1) = {8, 7};
//+
Line(4) = {5, 8};
//+
Line(30000000) = {5, 1};
//+
Line(10000000) = {2, 6};
//+
Line(9) = {3, 7};
//+
Line(20000000) = {8, 4};
//+
Line Loop(13) = {9, 2, -10000000, -5};
//+
Plane Surface(14) = {13};
//+
Line Loop(15) = {1, -9, -8, -20000000};
//+
Plane Surface(16) = {15};
//+
Line Loop(17) = {5, 6, 7, 8};
//+
Plane Surface(18) = {17};
//+
Line Loop(19) = {3, 30000000, -6, 10000000};
//+
Plane Surface(20) = {19};
//+
Line Loop(21) = {30000000, 7, -20000000, -4};
//+
Plane Surface(22) = {21};
//+
Line Loop(23) ={2, 3, 4, 1};
//+
Plane Surface(24) = {-23};
//+
Surface Loop(25) = {24, 14, 16, 18, 20, 22};
//+
Volume(26) = {25};
//+
Physical Surface(27) = {16};
//+
Physical Surface(28) = {20};
//+
Physical Volume(29) = {26};
//+
Physical Surface(30) = {24};
Physical Surface(31) = {18};



Field[5] = Distance;
Field[5].EdgesList = {5,6,7,8};
Field[5].NNodesByEdge = 10;
Field[5].NodesList = {9};
Field[6] = Threshold;
Field[6].IField = 5;
Field[6].LcMin = 0.15;
Field[6].LcMax = 1;
Field[6].DistMin = 1;
Field[6].DistMax = 2;
Background Field = 6;


//+





Field[7] = Distance;
Field[7].FacesList = {17,18};
Field[7].NNodesByEdge = 10;

Field[8] = Threshold;
Field[8].IField = 5;
Field[8].LcMin = 0.3;
Field[8].LcMax = 1;
Field[8].DistMin = 0.03;
Field[8].DistMax = 2;



//+
Field[9] = Min;
//+
Field[9].FieldsList = {8,6};

Background Field = 9;
//+

