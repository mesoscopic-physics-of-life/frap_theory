//+
Point(5) = {0, 2.4, 0, 1.0};
//+
Point(6) = {5, 2.4, 0, 1.0};
//+
Point(7) = {5, 2.6, 0, 1.0};
//+
Point(8) = {0, 2.6, 0, 1.0};

//+
Line(1) = {8, 5};
//+
Line(2) = {5, 6};
//+
Line(3) = {6, 7};
//+
Line(4) = {7, 8};



//+
Line Loop(10) = {1, 2, 3, 4};
//+
Plane Surface(11) = {10};
//+
Physical Surface(12) = {11};
//+
Physical Line(13) = {1, 4, 3, 2};






Field[5] = Distance;
Field[5].FacesList = {11};
Field[5].NNodesByEdge = 100;
Field[6] = Threshold;
Field[6].IField = 5;
Field[6].LcMin = 0.01;
Field[6].LcMax = 0.5;
Field[6].DistMin = 10;
Field[6].DistMax = 15;



Background Field = 6;
//+

