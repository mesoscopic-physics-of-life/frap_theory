% Attempt to implement effective droplet modle. For now doesn't seem to
% work, since flux can't be properly rescaled at boundary. See notes in
% PDE Solver note from 24/11/2021.
x = [linspace(0, 0.5, 300), linspace(0.5001, 1, 300)];
t = [0 0.001 0.005 0.01 0.05 0.1 0.5 1];
m = 0;
sol = pdepe(m,@pdex2pde,@pdex2ic,@pdex2bc,x,t);
u = sol(:,:,1);
% surf(x,t,u)
hold on;
% title('Numerical solution with nonuniform mesh')
% xlabel('Distance x')
% ylabel('Time t')
% zlabel('Solution u')
plot(x,u,x,u, 'b')
xlabel('Distance x')
ylabel('Solution u')
title('Solution profiles at several times')
function [c,f,s] = pdex2pde(x,t,u,dudx) % Equation to solve
c = 1;
s = 0;
if x <= 0.495
    f = dudx;
elseif x >= 0.505
    f = 5*dudx;
else
    % This can confer interfacial resistance
    f = 0.100000000*dudx;
end
end
%----------------------------------------------
function u0 = pdex2ic(x) %Initial conditions
if x < 0.5
    u0 = 0;
else
    u0 = 1;
end
end
%----------------------------------------------
function [pl,ql,pr,qr] = pdex2bc(xl,ul,xr,ur,t) % Boundary conditions
pl = 0;
ql = 1;
pr = 0;
qr = 1;
end
%----------------------------------------------