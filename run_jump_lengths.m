function [D_ratio, m] = run_jump_lengths(a, ad, chi, direc, l_max,...
                                    name, nu, t_ind, u0, u_g0)
% Calculate jump length distribution for given parameter set.
% ad ... prefactor of quadratic term in Stefano's mobility ansatz or e_g0
%        if mode is 'Mobility ratio'
b = @(chi, nu) nu^(1/3)*sqrt(chi/(chi-2));
e = @(chi) sqrt(3/8*(chi-2));
params = {a, b(chi, nu), u0, e(chi), ad, u_g0, 10, 7, 0, 0, 'Mobility_ratio', 0};
t = [0, 0.05, 0.1, 1];
x0 = sort(5-direc*(0:0.001:4.01));
%% Run simulations for 'delta' IC across outside
T = {};
parfor i = 1:length(x0)
    tic
    T{i} = Ternary_model(0, 'Gauss', params, t, 0.2);
    T{i}.x0 = x0(i);
    T{i}.solve_tern_frap();
    toc
end
%% Calculate probabilities for each jump length in ls.
ls = -direc*(0.000:0.001:l_max);
p = nan(1, length(ls));
tic
parfor i = 1:length(ls)
    tic
    p(i) = int_prob(ls(i), T, x0, direc, t_ind, 5, 0, 0);
    toc
end
toc
%% Normalization factor
% N = normalization(T, x0, 0, t_ind, direc, -params{1});
% should sum to one
N = sum(p)/length(p)*l_max;
m = sum(ls.*p/N)/length(p)*l_max;
T{1}.calc_real_params();
D_ratio = T{1}.real_params.D_out/T{1}.real_params.D_in;
save(name);
disp([D_ratio, m]);