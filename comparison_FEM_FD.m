%% Use below (this is now in radial cooordinates) for comparison with Python.
nu = 10^-6;
chi = 7/3;
b = nu^(1/3)*sqrt(chi/(chi-2));
e = sqrt(3/8*(chi-2));
T = Ternary_model(0, 'FRAP', [-1, b, 0.5, e, 1, 0.5, 300], linspace(0.0, 400, 5000), 2);
T.t = 0:0.1:10;
T.solve_tern_frap()
%%
csvwrite('resultsmatlab.txt', T.sol);
csvwrite('meshematlab.txt', T.x);
csvwrite('phi_tot.txt', T.phi_t);

% writematrix(T.sol, 'resultsmatlab.txt');
% % csvwrite('meshematlab.txt', T.x);